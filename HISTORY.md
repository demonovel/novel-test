# HISTORY

## 2019-01-25

### Epub

- [在喪屍橫行的世界裡唯獨我不被襲擊](h/%E5%9C%A8%E5%96%AA%E5%B1%8D%E6%A9%AB%E8%A1%8C%E7%9A%84%E4%B8%96%E7%95%8C%E8%A3%A1%E5%94%AF%E7%8D%A8%E6%88%91%E4%B8%8D%E8%A2%AB%E8%A5%B2%E6%93%8A) - h
  <br/>( v: 3 , c: 62, add: 0 )
- [航宙軍士官、冒険者になる](syosetu/%E8%88%AA%E5%AE%99%E8%BB%8D%E5%A3%AB%E5%AE%98%E3%80%81%E5%86%92%E9%99%BA%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8B) - syosetu
  <br/>( v: 1 , c: 9, add: 0 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( v: 3 , c: 25, add: 0 )
- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( v: 11 , c: 97, add: 97 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( v: 1 , c: 9, add: 0 )
- [被死神撫養的少女懷抱漆黑之劍](wenku8/%E8%A2%AB%E6%AD%BB%E7%A5%9E%E6%92%AB%E9%A4%8A%E7%9A%84%E5%B0%91%E5%A5%B3%E6%87%B7%E6%8A%B1%E6%BC%86%E9%BB%91%E4%B9%8B%E5%8A%8D) - wenku8
  <br/>( v: 3 , c: 19, add: 19 )

### Segment

- [在喪屍橫行的世界裡唯獨我不被襲擊](h/%E5%9C%A8%E5%96%AA%E5%B1%8D%E6%A9%AB%E8%A1%8C%E7%9A%84%E4%B8%96%E7%95%8C%E8%A3%A1%E5%94%AF%E7%8D%A8%E6%88%91%E4%B8%8D%E8%A2%AB%E8%A5%B2%E6%93%8A) - h
  <br/>( s: 13 )
- [航宙軍士官、冒険者になる](syosetu/%E8%88%AA%E5%AE%99%E8%BB%8D%E5%A3%AB%E5%AE%98%E3%80%81%E5%86%92%E9%99%BA%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8B) - syosetu
  <br/>( s: 2 )
- [四度目は嫌な死属性魔術師](user/%E5%9B%9B%E5%BA%A6%E7%9B%AE%E3%81%AF%E5%AB%8C%E3%81%AA%E6%AD%BB%E5%B1%9E%E6%80%A7%E9%AD%94%E8%A1%93%E5%B8%AB) - user
  <br/>( s: 14 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( s: 2 )
- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( s: 97 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( s: 1 )
- [被死神撫養的少女懷抱漆黑之劍](wenku8/%E8%A2%AB%E6%AD%BB%E7%A5%9E%E6%92%AB%E9%A4%8A%E7%9A%84%E5%B0%91%E5%A5%B3%E6%87%B7%E6%8A%B1%E6%BC%86%E9%BB%91%E4%B9%8B%E5%8A%8D) - wenku8
  <br/>( s: 9 )

## 2019-01-24

### Epub

- [不是真正同伴的我被逐出了勇者隊伍，因此決定在邊境過上慢生活](dmzj_out/%E4%B8%8D%E6%98%AF%E7%9C%9F%E6%AD%A3%E5%90%8C%E4%BC%B4%E7%9A%84%E6%88%91%E8%A2%AB%E9%80%90%E5%87%BA%E4%BA%86%E5%8B%87%E8%80%85%E9%9A%8A%E4%BC%8D%EF%BC%8C%E5%9B%A0%E6%AD%A4%E6%B1%BA%E5%AE%9A%E5%9C%A8%E9%82%8A%E5%A2%83%E9%81%8E%E4%B8%8A%E6%85%A2%E7%94%9F%E6%B4%BB) - dmzj_out
  <br/>( v: 2 , c: 21, add: 1 )
- [絕對想被打倒的魔王＆絕對不想戰鬥的勇者](other/%E7%B5%95%E5%B0%8D%E6%83%B3%E8%A2%AB%E6%89%93%E5%80%92%E7%9A%84%E9%AD%94%E7%8E%8B%EF%BC%86%E7%B5%95%E5%B0%8D%E4%B8%8D%E6%83%B3%E6%88%B0%E9%AC%A5%E7%9A%84%E5%8B%87%E8%80%85) - other
  <br/>( v: 1 , c: 4, add: 0 )
- [航宙軍士官、冒険者になる](syosetu/%E8%88%AA%E5%AE%99%E8%BB%8D%E5%A3%AB%E5%AE%98%E3%80%81%E5%86%92%E9%99%BA%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8B) - syosetu
  <br/>( v: 1 , c: 9, add: 9 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user_out/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user_out
  <br/>( v: 7 , c: 170, add: 1 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( v: 3 , c: 25, add: 0 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( v: 1 , c: 9, add: 0 )

### Segment

- [航宙軍士官、冒険者になる](syosetu/%E8%88%AA%E5%AE%99%E8%BB%8D%E5%A3%AB%E5%AE%98%E3%80%81%E5%86%92%E9%99%BA%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8B) - syosetu
  <br/>( s: 9 )
- [四度目は嫌な死属性魔術師](user/%E5%9B%9B%E5%BA%A6%E7%9B%AE%E3%81%AF%E5%AB%8C%E3%81%AA%E6%AD%BB%E5%B1%9E%E6%80%A7%E9%AD%94%E8%A1%93%E5%B8%AB) - user
  <br/>( s: 18 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user
  <br/>( s: 2 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( s: 2 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( s: 3 )

## 2019-01-23

### Epub

- [不是真正同伴的我被逐出了勇者隊伍，因此決定在邊境過上慢生活](dmzj_out/%E4%B8%8D%E6%98%AF%E7%9C%9F%E6%AD%A3%E5%90%8C%E4%BC%B4%E7%9A%84%E6%88%91%E8%A2%AB%E9%80%90%E5%87%BA%E4%BA%86%E5%8B%87%E8%80%85%E9%9A%8A%E4%BC%8D%EF%BC%8C%E5%9B%A0%E6%AD%A4%E6%B1%BA%E5%AE%9A%E5%9C%A8%E9%82%8A%E5%A2%83%E9%81%8E%E4%B8%8A%E6%85%A2%E7%94%9F%E6%B4%BB) - dmzj_out
  <br/>( v: 2 , c: 20, add: 3 )
- [勇者「都打倒魔王了來回家吧」](other/%E5%8B%87%E8%80%85%E3%80%8C%E9%83%BD%E6%89%93%E5%80%92%E9%AD%94%E7%8E%8B%E4%BA%86%E4%BE%86%E5%9B%9E%E5%AE%B6%E5%90%A7%E3%80%8D) - other
  <br/>( v: 1 , c: 3, add: 0 )
- [絕對想被打倒的魔王＆絕對不想戰鬥的勇者](other/%E7%B5%95%E5%B0%8D%E6%83%B3%E8%A2%AB%E6%89%93%E5%80%92%E7%9A%84%E9%AD%94%E7%8E%8B%EF%BC%86%E7%B5%95%E5%B0%8D%E4%B8%8D%E6%83%B3%E6%88%B0%E9%AC%A5%E7%9A%84%E5%8B%87%E8%80%85) - other
  <br/>( v: 1 , c: 4, add: 4 )
- [四度目は嫌な死属性魔術師](user_out/%E5%9B%9B%E5%BA%A6%E7%9B%AE%E3%81%AF%E5%AB%8C%E3%81%AA%E6%AD%BB%E5%B1%9E%E6%80%A7%E9%AD%94%E8%A1%93%E5%B8%AB) - user_out
  <br/>( v: 18 , c: 279, add: 1 )
- [回復術士のやり直し～即死魔法とスキルコピーの超越ヒール～](user_out/%E5%9B%9E%E5%BE%A9%E8%A1%93%E5%A3%AB%E3%81%AE%E3%82%84%E3%82%8A%E7%9B%B4%E3%81%97%EF%BD%9E%E5%8D%B3%E6%AD%BB%E9%AD%94%E6%B3%95%E3%81%A8%E3%82%B9%E3%82%AD%E3%83%AB%E3%82%B3%E3%83%94%E3%83%BC%E3%81%AE%E8%B6%85%E8%B6%8A%E3%83%92%E3%83%BC%E3%83%AB%EF%BD%9E) - user_out
  <br/>( v: 7 , c: 143, add: 0 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( v: 3 , c: 25, add: 25 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( v: 1 , c: 9, add: 0 )

### Segment

- [絕對想被打倒的魔王＆絕對不想戰鬥的勇者](other/%E7%B5%95%E5%B0%8D%E6%83%B3%E8%A2%AB%E6%89%93%E5%80%92%E7%9A%84%E9%AD%94%E7%8E%8B%EF%BC%86%E7%B5%95%E5%B0%8D%E4%B8%8D%E6%83%B3%E6%88%B0%E9%AC%A5%E7%9A%84%E5%8B%87%E8%80%85) - other
  <br/>( s: 2 )
- [四度目は嫌な死属性魔術師](user/%E5%9B%9B%E5%BA%A6%E7%9B%AE%E3%81%AF%E5%AB%8C%E3%81%AA%E6%AD%BB%E5%B1%9E%E6%80%A7%E9%AD%94%E8%A1%93%E5%B8%AB) - user
  <br/>( s: 1 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( s: 25 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( s: 7 )

## 2019-01-22

### Epub

- [帰ってきた元勇者](syosetu_out/%E5%B8%B0%E3%81%A3%E3%81%A6%E3%81%8D%E3%81%9F%E5%85%83%E5%8B%87%E8%80%85) - syosetu_out
  <br/>( v: 36 , c: 240, add: 0 )
- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 62, add: 14 )

## 2019-01-21

### Epub

- [リビティウム皇国のブタクサ姫](cm_out/%E3%83%AA%E3%83%93%E3%83%86%E3%82%A3%E3%82%A6%E3%83%A0%E7%9A%87%E5%9B%BD%E3%81%AE%E3%83%96%E3%82%BF%E3%82%AF%E3%82%B5%E5%A7%AB) - cm_out
  <br/>( v: 7 , c: 209, add: 1 )
- [只有無職是不會辭掉的](mirronight/%E5%8F%AA%E6%9C%89%E7%84%A1%E8%81%B7%E6%98%AF%E4%B8%8D%E6%9C%83%E8%BE%AD%E6%8E%89%E7%9A%84) - mirronight
  <br/>( v: 8 , c: 190, add: 190 )
- [ウォルテニア戦記](user_out/%E3%82%A6%E3%82%A9%E3%83%AB%E3%83%86%E3%83%8B%E3%82%A2%E6%88%A6%E8%A8%98) - user_out
  <br/>( v: 5 , c: 177, add: 3 )

### Segment

- [只有無職是不會辭掉的](mirronight/%E5%8F%AA%E6%9C%89%E7%84%A1%E8%81%B7%E6%98%AF%E4%B8%8D%E6%9C%83%E8%BE%AD%E6%8E%89%E7%9A%84) - mirronight
  <br/>( s: 1 )

## 2019-01-20

### Epub

- [侯爵嫡男好色物語](h/%E4%BE%AF%E7%88%B5%E5%AB%A1%E7%94%B7%E5%A5%BD%E8%89%B2%E7%89%A9%E8%AA%9E) - h
  <br/>( v: 1 , c: 47, add: 47 )
- [在喪屍橫行的世界裡唯獨我不被襲擊](h/%E5%9C%A8%E5%96%AA%E5%B1%8D%E6%A9%AB%E8%A1%8C%E7%9A%84%E4%B8%96%E7%95%8C%E8%A3%A1%E5%94%AF%E7%8D%A8%E6%88%91%E4%B8%8D%E8%A2%AB%E8%A5%B2%E6%93%8A) - h
  <br/>( v: 3 , c: 62, add: 62 )
- [異世界の男は全滅したようです](h/%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%AE%E7%94%B7%E3%81%AF%E5%85%A8%E6%BB%85%E3%81%97%E3%81%9F%E3%82%88%E3%81%86%E3%81%A7%E3%81%99) - h
  <br/>( v: 2 , c: 33, add: 33 )
- [勇者、辞めます　～次の職場は魔王城～](kakuyomu/%E5%8B%87%E8%80%85%E3%80%81%E8%BE%9E%E3%82%81%E3%81%BE%E3%81%99%E3%80%80%EF%BD%9E%E6%AC%A1%E3%81%AE%E8%81%B7%E5%A0%B4%E3%81%AF%E9%AD%94%E7%8E%8B%E5%9F%8E%EF%BD%9E) - kakuyomu
  <br/>( v: 4 , c: 10, add: 10 )
- [在異世界轉移從女神大人那兒得到祝福](mirronight/%E5%9C%A8%E7%95%B0%E4%B8%96%E7%95%8C%E8%BD%89%E7%A7%BB%E5%BE%9E%E5%A5%B3%E7%A5%9E%E5%A4%A7%E4%BA%BA%E9%82%A3%E5%85%92%E5%BE%97%E5%88%B0%E7%A5%9D%E7%A6%8F) - mirronight
  <br/>( v: 2 , c: 40, add: 40 )
- [奴隷商人しか選択肢がないですよ？　～ハーレム？　なにそれおいしいの？～](syosetu/%E5%A5%B4%E9%9A%B7%E5%95%86%E4%BA%BA%E3%81%97%E3%81%8B%E9%81%B8%E6%8A%9E%E8%82%A2%E3%81%8C%E3%81%AA%E3%81%84%E3%81%A7%E3%81%99%E3%82%88%EF%BC%9F%E3%80%80%EF%BD%9E%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%EF%BC%9F%E3%80%80%E3%81%AA%E3%81%AB%E3%81%9D%E3%82%8C%E3%81%8A%E3%81%84%E3%81%97%E3%81%84%E3%81%AE%EF%BC%9F%EF%BD%9E) - syosetu
  <br/>( v: 1 , c: 1, add: 1 )
- [望まぬ不死の冒険者](syosetu_out/%E6%9C%9B%E3%81%BE%E3%81%AC%E4%B8%8D%E6%AD%BB%E3%81%AE%E5%86%92%E9%99%BA%E8%80%85) - syosetu_out
  <br/>( v: 10 , c: 188, add: 11 )
- [俺の死亡フラグが留まるところを知らない](user_out/%E4%BF%BA%E3%81%AE%E6%AD%BB%E4%BA%A1%E3%83%95%E3%83%A9%E3%82%B0%E3%81%8C%E7%95%99%E3%81%BE%E3%82%8B%E3%81%A8%E3%81%93%E3%82%8D%E3%82%92%E7%9F%A5%E3%82%89%E3%81%AA%E3%81%84) - user_out
  <br/>( v: 5 , c: 104, add: 104 )
- [呪術師は勇者になれない](user_out/%E5%91%AA%E8%A1%93%E5%B8%AB%E3%81%AF%E5%8B%87%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8C%E3%81%AA%E3%81%84) - user_out
  <br/>( v: 2 , c: 10, add: 10 )
- [破壊の御子](user_out/%E7%A0%B4%E5%A3%8A%E3%81%AE%E5%BE%A1%E5%AD%90) - user_out
  <br/>( v: 1 , c: 71, add: 71 )
- [關於人類是最強種族這件事](wenku8/%E9%97%9C%E6%96%BC%E4%BA%BA%E9%A1%9E%E6%98%AF%E6%9C%80%E5%BC%B7%E7%A8%AE%E6%97%8F%E9%80%99%E4%BB%B6%E4%BA%8B) - wenku8
  <br/>( v: 4 , c: 52, add: 52 )

### Segment

- [侯爵嫡男好色物語](h/%E4%BE%AF%E7%88%B5%E5%AB%A1%E7%94%B7%E5%A5%BD%E8%89%B2%E7%89%A9%E8%AA%9E) - h
  <br/>( s: 1 )
- [在喪屍橫行的世界裡唯獨我不被襲擊](h/%E5%9C%A8%E5%96%AA%E5%B1%8D%E6%A9%AB%E8%A1%8C%E7%9A%84%E4%B8%96%E7%95%8C%E8%A3%A1%E5%94%AF%E7%8D%A8%E6%88%91%E4%B8%8D%E8%A2%AB%E8%A5%B2%E6%93%8A) - h
  <br/>( s: 60 )
- [異世界の男は全滅したようです](h/%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%AE%E7%94%B7%E3%81%AF%E5%85%A8%E6%BB%85%E3%81%97%E3%81%9F%E3%82%88%E3%81%86%E3%81%A7%E3%81%99) - h
  <br/>( s: 32 )
- [勇者、辞めます　～次の職場は魔王城～](kakuyomu/%E5%8B%87%E8%80%85%E3%80%81%E8%BE%9E%E3%82%81%E3%81%BE%E3%81%99%E3%80%80%EF%BD%9E%E6%AC%A1%E3%81%AE%E8%81%B7%E5%A0%B4%E3%81%AF%E9%AD%94%E7%8E%8B%E5%9F%8E%EF%BD%9E) - kakuyomu
  <br/>( s: 9 )
- [黑之魔王](user/%E9%BB%91%E4%B9%8B%E9%AD%94%E7%8E%8B) - user
  <br/>( s: 1 )
- [關於人類是最強種族這件事](wenku8/%E9%97%9C%E6%96%BC%E4%BA%BA%E9%A1%9E%E6%98%AF%E6%9C%80%E5%BC%B7%E7%A8%AE%E6%97%8F%E9%80%99%E4%BB%B6%E4%BA%8B) - wenku8
  <br/>( s: 52 )

## 2019-01-19

### Epub

- [暇人、魔王の姿で異世界へ](syosetu_out/%E6%9A%87%E4%BA%BA%E3%80%81%E9%AD%94%E7%8E%8B%E3%81%AE%E5%A7%BF%E3%81%A7%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%B8) - syosetu_out
  <br/>( v: 10 , c: 225, add: 1 )
- [陰の実力者になりたくて！_(n0611em)](syosetu_out/%E9%99%B0%E3%81%AE%E5%AE%9F%E5%8A%9B%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%9F%E3%81%8F%E3%81%A6%EF%BC%81_(n0611em)) - syosetu_out
  <br/>( v: 9 , c: 170, add: 4 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user_out/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user_out
  <br/>( v: 7 , c: 169, add: 2 )
- [黑之魔王](user_out/%E9%BB%91%E4%B9%8B%E9%AD%94%E7%8E%8B) - user_out
  <br/>( v: 29 , c: 556, add: 4 )

### Segment

- [豚公爵に転生したから、今度は君に好きと言いたい](user/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user
  <br/>( s: 2 )
- [黑之魔王](user/%E9%BB%91%E4%B9%8B%E9%AD%94%E7%8E%8B) - user
  <br/>( s: 5 )

## 2019-01-18

### Epub

- [在異世界轉移從女人大人那兒得到祝福](mirronight/%E5%9C%A8%E7%95%B0%E4%B8%96%E7%95%8C%E8%BD%89%E7%A7%BB%E5%BE%9E%E5%A5%B3%E4%BA%BA%E5%A4%A7%E4%BA%BA%E9%82%A3%E5%85%92%E5%BE%97%E5%88%B0%E7%A5%9D%E7%A6%8F) - mirronight
  <br/>( v: 2 , c: 40, add: 0 )
- [月桂樹の唄](syosetu/%E6%9C%88%E6%A1%82%E6%A8%B9%E3%81%AE%E5%94%84) - syosetu
  <br/>( v: 3 , c: 14, add: 0 )
- [變成了美少女、但也成了網游廢人。](syosetu/%E8%AE%8A%E6%88%90%E4%BA%86%E7%BE%8E%E5%B0%91%E5%A5%B3%E3%80%81%E4%BD%86%E4%B9%9F%E6%88%90%E4%BA%86%E7%B6%B2%E6%B8%B8%E5%BB%A2%E4%BA%BA%E3%80%82) - syosetu
  <br/>( v: 1 , c: 9, add: 0 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( v: 1 , c: 9, add: 0 )

## 2019-01-17

### Epub

- [リビティウム皇国のブタクサ姫](cm_out/%E3%83%AA%E3%83%93%E3%83%86%E3%82%A3%E3%82%A6%E3%83%A0%E7%9A%87%E5%9B%BD%E3%81%AE%E3%83%96%E3%82%BF%E3%82%AF%E3%82%B5%E5%A7%AB) - cm_out
  <br/>( v: 7 , c: 208, add: 1 )
- [在異世界轉移從女人大人那兒得到祝福](mirronight/%E5%9C%A8%E7%95%B0%E4%B8%96%E7%95%8C%E8%BD%89%E7%A7%BB%E5%BE%9E%E5%A5%B3%E4%BA%BA%E5%A4%A7%E4%BA%BA%E9%82%A3%E5%85%92%E5%BE%97%E5%88%B0%E7%A5%9D%E7%A6%8F) - mirronight
  <br/>( v: 2 , c: 40, add: 20 )
- [月桂樹の唄](syosetu/%E6%9C%88%E6%A1%82%E6%A8%B9%E3%81%AE%E5%94%84) - syosetu
  <br/>( v: 3 , c: 14, add: 14 )
- [變成了美少女、但也成了網游廢人。](syosetu/%E8%AE%8A%E6%88%90%E4%BA%86%E7%BE%8E%E5%B0%91%E5%A5%B3%E3%80%81%E4%BD%86%E4%B9%9F%E6%88%90%E4%BA%86%E7%B6%B2%E6%B8%B8%E5%BB%A2%E4%BA%BA%E3%80%82) - syosetu
  <br/>( v: 1 , c: 9, add: 9 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( v: 1 , c: 9, add: 9 )

### Segment

- [在異世界轉移從女人大人那兒得到祝福](mirronight/%E5%9C%A8%E7%95%B0%E4%B8%96%E7%95%8C%E8%BD%89%E7%A7%BB%E5%BE%9E%E5%A5%B3%E4%BA%BA%E5%A4%A7%E4%BA%BA%E9%82%A3%E5%85%92%E5%BE%97%E5%88%B0%E7%A5%9D%E7%A6%8F) - mirronight
  <br/>( s: 9 )
- [アラフォー賢者の異世界生活日記](syosetu/%E3%82%A2%E3%83%A9%E3%83%95%E3%82%A9%E3%83%BC%E8%B3%A2%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E7%94%9F%E6%B4%BB%E6%97%A5%E8%A8%98) - syosetu
  <br/>( s: 6 )
- [月桂樹の唄](syosetu/%E6%9C%88%E6%A1%82%E6%A8%B9%E3%81%AE%E5%94%84) - syosetu
  <br/>( s: 14 )
- [變成了美少女、但也成了網游廢人。](syosetu/%E8%AE%8A%E6%88%90%E4%BA%86%E7%BE%8E%E5%B0%91%E5%A5%B3%E3%80%81%E4%BD%86%E4%B9%9F%E6%88%90%E4%BA%86%E7%B6%B2%E6%B8%B8%E5%BB%A2%E4%BA%BA%E3%80%82) - syosetu
  <br/>( s: 9 )
- [打倒女神勇者的下流手段](wenku8/%E6%89%93%E5%80%92%E5%A5%B3%E7%A5%9E%E5%8B%87%E8%80%85%E7%9A%84%E4%B8%8B%E6%B5%81%E6%89%8B%E6%AE%B5) - wenku8
  <br/>( s: 2 )

## 2019-01-16

### Epub

- [異世界転移で女神様から祝福を！　～いえ、手持ちの異能があるので結構です～](cm/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BB%A2%E7%A7%BB%E3%81%A7%E5%A5%B3%E7%A5%9E%E6%A7%98%E3%81%8B%E3%82%89%E7%A5%9D%E7%A6%8F%E3%82%92%EF%BC%81%E3%80%80%EF%BD%9E%E3%81%84%E3%81%88%E3%80%81%E6%89%8B%E6%8C%81%E3%81%A1%E3%81%AE%E7%95%B0%E8%83%BD%E3%81%8C%E3%81%82%E3%82%8B%E3%81%AE%E3%81%A7%E7%B5%90%E6%A7%8B%E3%81%A7%E3%81%99%EF%BD%9E) - cm
  <br/>( v: 5 , c: 102, add: 0 )
- [在異世界轉移從女人大人那兒得到祝福](mirronight/%E5%9C%A8%E7%95%B0%E4%B8%96%E7%95%8C%E8%BD%89%E7%A7%BB%E5%BE%9E%E5%A5%B3%E4%BA%BA%E5%A4%A7%E4%BA%BA%E9%82%A3%E5%85%92%E5%BE%97%E5%88%B0%E7%A5%9D%E7%A6%8F) - mirronight
  <br/>( v: 1 , c: 20, add: 0 )

### Segment

- [アラフォー賢者の異世界生活日記](syosetu/%E3%82%A2%E3%83%A9%E3%83%95%E3%82%A9%E3%83%BC%E8%B3%A2%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E7%94%9F%E6%B4%BB%E6%97%A5%E8%A8%98) - syosetu
  <br/>( s: 26 )
- [魔王様、リトライ！](user/%E9%AD%94%E7%8E%8B%E6%A7%98%E3%80%81%E3%83%AA%E3%83%88%E3%83%A9%E3%82%A4%EF%BC%81) - user
  <br/>( s: 7 )



