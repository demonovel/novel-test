
关於勇者开始的故事，首先不从他的幼年期开始说明就不行吧。

在他年幼的时候，人间界马尔库特的情势很严峻不安定。
不如说，是由於魔族多次的袭击，导致一点一点地变得艰苦。

但是，魔族明明是拥有者明显战力差的，却不一口气攻过来。
当时的魔王在玩弄着人类。


在那样状况下诞生的勇者，从幼小的时候就已经被当做训练兵征收了。


才能也就普通程度，当时的他并不突出也没有什麼优秀之处。
倒不如说，他的兄长，正是後来的魔法使，那边在幼年期的时候收到好评。


[俺啊，将来可是要作为魔法使守护去勇者的背後哒！总有一天，绝对要进入勇者队伍]


幼时的勇者是追逐着兄长的魔法使的背影长大的。

老实地说，他并不喜欢战鬥。但是，为了变得像兄长那般帅气，被影响着努力地训练着。

与青梅竹马的僧侣一起，为了得到兄长些许的认同而努力着。

但是，战况并没有好到能让那样的少年们慢慢来。

勇者，魔法使，僧侣一旦有些许成长後，立马就被驱使往战场了。

对手基本是弱小的魔族。但是，没有实力的三人常常会受到生命危险。

但是，三人合力将其克服了。互相成长着，在任何苦难前面都不逃避面对着。

不久，三人的队伍开始出名了。那之後没多久就引起了王女様的关注了。


[你们是下一代的希望——一起，为之努力吧！]


在这段时间点，队伍的成员增加了。
得到了後辈，之後战士与武斗家也加入了，队伍也越来越活跃了。
与队伍的同伴深交也是这个时期。

是以後的勇者，一直珍视着的东西……那个时候的，美好的记忆。

大家都向着一个目标迈进着。
保持着步调。以稍快的速度，开始成为人间界屈指可数的实力者了。

接着在这之後，勇者作为勇者的觉醒开始了。
为了守护最重要的同伴……他以那个心情，蹭蹭地变强了。

锻炼着後辈的战士与武斗家，变得理所当然地守护着後卫的魔法使与僧侣，最终以勇者成为了队伍最重要的位置了。

那件事情，对於在幼年期被称作天才的魔法使来说，并不有趣。
幸福的队伍，与勇者的觉醒一起，产生了裂痕。

裂隙随时间的经过扩大，最终成为了只有勇者突出的不协调队伍了。

魔法使闹气别扭了。僧侣因成为了魔法使与勇者的夹板而成天提心吊胆。後辈的两人在名为勇者的怪物面前放弃了努力，队伍的氛围变得险恶了。

这使得王女様也不接近队伍了，接着她每日在别的地方散发压力——也就是，变得寻求男人了。

这是由於当时猪一样卑劣的王女様的父亲，是当时的王的原因也说不定。
但是，王女様堕落了也是事实。

勇者想要守护的最珍重的东西，不知不觉间就溃散了。
若要说个原因的話，那就是每个人的内心软弱除此无他了吧。


在勇者这一耀眼的光芒面目，他们把视线移开了。


明明是这样的，勇者却把那当做是自己的过错。

（是我将其毁坏的）


在这个时候第一次，对於自己是勇者之事感到了後悔。

自我谴责，自我反省，自我克制……即使这样，相信着同伴们会回到原样，勇者更加努力地努力着。


他们不做的那部分，勇者全部去做了。
他们不背负的责任，勇者都背负了。


在这过程中，成为关键转折点的——果然是与前任魔王的决战吧。

当时的勇者被灭绝，幾乎失去战力的人类变得自暴自弃了。
但是只有成为了新任勇者的他没有放弃，挺身面对魔王了。


谁都认为会输的。认为敌不过的就放弃了。

但是，他——想要守护住同伴们可以回去的地方。
仅为了这种理由，勇者就把前任魔王给打倒了。
(这下子，又可以……和大家）


取得胜利的勇者，‘在背後的同伴们说不能能给予胜利的祝福也说不定’，满怀期待地回过头。


但是，後面那裡……谁都不在。
大家早就抛弃勇者逃跑了。
(……也是呢)


浮现出自嘲笑容的他，踏过前任魔王的尸骸，打算就此离去。

那时，他与她相遇了。

[勇者——我是下任的魔王哒。请多多关照哦]
(……有什麼好笑的啊)


第一眼见到，勇者就对这个幼女变得讨厌了。
在战场上看到笑着的她让人烦躁。


对他来说，战鬥即是互相厮杀。
并不是能让人發笑的东西。对於对这种事还能笑出来的幼女魔王，他很厌恶。


外表是很可爱的。但是，也只是这样了。


这麼袭击过来的話就杀死，勇者散发着杀气这麼说道。

但是，她居然扫兴地立马就回去了。

（真是奇怪的傢伙呐……感觉脸都变红了，难道是感冒了麼？）


不管怎麼说，魔王是击退了。
平安无事地守护住了人类。


这下子，大家应该就能再次向前了吧——这麼想着的。
(……果然，么)

什麼都，没有改变。


勇者变得连在队伍中也没有立足之地了，猪一样的王那裡也得不到报酬，最终变成了连民众都不愿相视的结果了。


讨厌这样的现实啊。
至少自己去努力的話，世界就会改变，的……如此相信着的。


因此勇者一直相信着，总有一天大家会注意到的，仅仅一人日夜战鬥着。


孤独地，与魔王君继续战鬥着。
在那个过程中，因为有勇者，所以训练学校被认为不需要，被破坏了。有本事的人变得幾乎不存在了。

同伴们也怠惰着度过每一天。

看到像金鱼粪那样跟在勇者周围，只寻求报酬的伙伴们……勇者变得无礼了。


在那之中，唯一能得到治癒的地方，偏偏却只有在战场。


对勇者来说，只有与幼女的魔王战鬥才是救赎。

魔王在战鬥的时候，总是会搭話过来。
没有意图的对話是这般快乐的东西么……让当时的勇者会这麼觉得。

【勇者哟，好像又变强了呐！】
【是不是个子长高了啊？成长期真是好事哒！】
【呐啊，勇者喜欢怎样类型的女孩子啊？不是，并没有其他一哟哦？】
【昨天的饭非常美味呐！不由得再添一碗了】
【今天真是好天气哒。偶尔悠闲地日光浴也不差】
【……勇者哟。要来杯茶么？啊，刚才的不算。不好意思呐，忘了吧】


——之类的，渐渐适当地说些会話内容了，但即便这样勇者也变得与魔王说話了。


住处，勇者是讨厌魔王的。


不过，在多次会面，交谈的过程中……不知不觉厌恶的感情消失了，好感般的东西取而代之地萌生了。


【你啊，和前任魔王不同呐……我觉得是个好家伙。你若不是魔王的話，大概,或许会变得友好吧】


不自觉地将想法脱口而出了。
如果，她是勇者的同伴的話……他肯定会喜欢到想要去结婚的吧。


喜欢上魔王，到了让他这麼觉得的程度。
只是，他是马尔库特的守护者。


即使他的身体损坏，也要一生守护着，如此發誓了。
说不定同伴们会变回原样的可能选哪个也有，所以……为了大家，勇者继续努力着。


加油着，努力着，奋鬥着。（頑張って、頑張って、頑張って。）【译：全用努力感觉没有感情呢~】

然後，在即将崩坏的时候……魔王说了这样的話。

【勇者哟。你的行为，真的是为了人类的么？】


他希望是好的，持续守护着。
但是，他的行为只是过分保护。


真实是，勇者的行为是谁都没有拯救。
只是，仅仅在娇惯着大家，毫无意义。

举个栗子，如果勇者死掉的話，人间界马上就会迎来终焉的吧。


注意到这个事实的勇者，大幅动摇了。

（我做错了……?）


自己的行为是错误的。
这麼想着，他突然地就感到空虚了。

至今为止所努力的到底是什麼，变得要放弃一切了。
那时，在他脑海内浮现的——正是魔王的身影。

在被王女様命令到是时候杀死魔王的第二天。
在以最终决战为名义，到访的魔王城裡，勇者心中抱持着迷茫与魔王会面了。


在那裡对我所说的，是这样的事情。

【给你世界的一半，所以不来与我一起么？】


对勇者自身……并不是头衔。也不是他所拥有的力量。
称作勇者的这个人类，正是魔王说想要的。


勇者此时，心就被魔王夺走了。

（在这傢伙旁边的話，我……）


可以变得幸福，如此确信了。


比起人类，比起伙伴。
勇者更想要与魔王在一起了。


就这样，他背叛了人间界。


於是，两人的命运交织在一起了。
勇者与魔王，在这无法相容的立场上，两人互相寻求，成为了一体。


对这个命运，勇者表示了感谢。
为了与魔王相遇，勇者才成为了勇者。


因为只要这麼想，他就不再会後悔成为勇者之事了——


————————完——————————

