去洞窟的路途很顺利。说是顺利，倒不如说之前预想的路途还更远一些。
他们三人，用马匹一口气从草原北上。这还是玛丽薇尔给出的提案。
刚开始，米蕾亚想要和第一次去挑战的时候一样，全员一起坐着马车去，但被玛丽薇尔敲着头说服了。
和其他的兄弟们比起来，己方已经落後了一天了。如果还用马车慢悠悠地前进的話，恐怕到达的时候试練就已经全都结束了。
所以，三人活用了人少的好处，骑马一口气赶路。因为其他的兄弟雇佣了很多护卫的冒险者，自然会因为配合步行速度而拖慢脚程。
利用那点，这边一口气缩短距离。理由都说明到了这种地步，米蕾亚也不得不点头同意了。就决定了移动的手段是骑马了。
但是，这裡产生了一个问题。玛丽薇尔和米蕾亚没什麼问题，但利安却不会骑马。
在深山的村裡成长的他，当然至今为止都没有过学习过马术。生为王族的两人会骑是肯定的，但利安连操控马的入门知识都没有。
玛丽薇尔对这个盲点感到头疼的时候，利安对她笑了笑仿佛没什麼大不了的说，【没问题的，我跑步就行了】。

他对两人说。比起现在不会骑马就先做练习所花费的世界，还是直接跑的还比较快，也有锻炼的因素在内。
对不带玩笑，认真说出那些話的利安，玛丽薇尔和米蕾亚的脸都不由得抽搐了。和马一起跑，开玩笑也要有个度啊。
对玛丽薇尔而言，在某种程度的距离赶路的話，比起马来说确实是跑的比较快。但是，那可是步行三天才能到的长距离的話，应该是绝无可能的。首先体力就撑不住了。
对此进行说明後，利安仍然用【没关係】这样毫无根據的自信告诉她们。那个样子，不知为何让玛丽薇尔联想到了之前飞向西方的笨蛋，对此她不由觉得自己的脑子是不是出了什麼问题。
结果，相信了利安的發言，变成了玛丽薇尔和米蕾亚骑马，利安则是跑步的發展了。
[莫非利安先生……并非人类吗]
[别多想了。我都已经不在意了。那已经是穿着非常识的衣服在跑了哟。]（PS：这裡不太清楚有什麼典故，估计是上一話的梗，强者=撒东那样穿着奇怪的衣服。也可能是龙珠的练功服梗）

不管在马上谈話的两人，利安气息平稳地在草原上跑过去。遥遥领先于两人的马匹。
大约出發了数小时後，利安依然没有一丝疲态。不仅如此，他还领先於马匹，担任了将路上挡道的魔物们清理掉的清道夫。
按玛丽薇尔的話来说，已经不仅仅是冲击的程度了。不仅背负着那样巨大的枪，还用比马还快的速度跑过去，将魔物一个个瞬间穿透。
那已经完全是非人了。对那个毫无道理的样子吃惊以外，玛丽薇尔还现出了米蕾亚从未见过的放鬆的表情。利安真的，太有趣了。
由於利安超於预料的行动，直到傍晚为止一天的行军进度，已经成功的超出了预期。
玛丽薇尔估算大概走了一半的距离吧。按照这个步伐的話，已经差不多补回了一天的差距了。
保证了水源後，搭建了过夜的野营地。利安正想决定轮番睡觉的顺序，玛丽薇尔以不需要为由否定了并指着米蕾亚说明道。
[这姑娘很擅长这类魔法，不需要警戒哦。拜托了，米蕾亚]
玛丽薇尔做出指示後，米蕾亚认命般地从包裡拿出装着液体的小瓶，向周围挥洒。
然後闭上眼，双手合起祈祷。
很快，米蕾亚站着的地方周围10步左右的範围的地面亮了起来，描绘出了一个大圈。在米蕾亚慢慢地张开眼睛的同时，地面放出的光消散了。
对那幻想般的景象迷住了的利安，玛丽薇尔露出仿佛恶作剧成功了的笑容缓缓说道。
[梅古阿克拉斯王国是力之国，初代的王以一己之力作为武器建造了国家。因此，作为王族不得不将自己培养成武器。也就是说，这应该就是米蕾亚所持的武器哟。这姑娘虽小但也是热心的莉莉夏教徒呢，这一类破邪或治癒的魔法是拿手之物。]
[这样啊……王族，真的很厲害呢。米蕾亚大人，非常感谢]
[没、没啥……没什麼大不了的。对我来说不算什麼。的确我如果不是作为王族出生就能选为女神莉莉夏的巫女但也掌握某种程度的神魔法啦]
[嘛，但是因为在兄弟之间却因为性格软弱而被当做笨蛋就是了，实在优秀的話就不会被谁抛下了。但因为性格软弱。很快就哭了]
[到、到底是夸我还是骂我请说清楚啊！]
[那就当做是笨蛋吧。一直以来你作为长女对别人说自己是废物的言论都沉默不语。要反驳的話就来打我呀，看着就心烦，你这胆小鬼]
[请、请说点好話吧……]
[你们俩，关係真的很好呢。就好像真的姐妹那样。说起来，发色也是一样的……总觉得脸也是]
[如果利安再继续说下去的話，就把你的枪丢到这条河裡去哦]
[别、别这样啊……如果被撒东大人知道搞丢了的話，我会被杀掉的]
在欢声笑语的谈笑中，三人迎来了夜晚。

晚饭的时候玛丽薇尔说了携带的食物不足的问题，利安说稍等一下就去草原上猎取了魔兽回来，玛丽薇尔只能硬着头皮去弄了。
或许是味道不好吧，魔兽烤肉一入了米蕾亚的嘴裡，她就铁青着脸跑去河边吐了出来。
然後，他们决定交替到水裡洗澡，玛丽薇尔和米蕾亚先行洗完後，就轮到利安去河边了。
因为利安走出去了之後，结界裡就剩下两人在裡面了。玛丽薇尔正考虑着要不要先睡觉，但听到了米蕾亚的一句嘀咕後，睡意就消失了。
[呐，玛丽薇尔……你之前，为什麼要离开王城？为什麼要放弃王位继承权呢？]
[说了要叫米库的吧。理由也对那个笨蛋国王说过了哦。对王位什麼的没有兴趣。我可不想被那种无聊的东西束缚着过完一生。我的剑，也不是为了杀掉你们而磨练的剑。很恶心啊。不管是以兄弟同胞之间的互相残杀来决定国家的命运什麼的，那个笨蛋王也是，什麼都一样。所以就跑出来了。我的人生是属於我自己的，我渴望按照自己的意志来活下去有什麼错]
[玛丽薇尔，真坚强呢……不愧是你啊。从以前开始，你就是那样了]
[下次再叫那个名字就在利安面前把你扒光了。总之你啊，为什麼要参加王位争夺战什麼的啊。明明是胆小的爱哭鬼，就没点像样的自觉吗？虽然我也差不多，你不也和上面的傢伙不一样，对王位没什麼兴趣么。即使到手了也没餘裕去应付，那麼你就快点掉队吧]
[不可能的啦……我，并没有玛丽薇尔那麼坚强哦。讨厌什麼的，说不出来的啦。因为我，就是被教育成那个样子的。因为从小时候开始，我们就是为了成为王而学习，去競争，继而成长的哦。那样的我们如果从王位争夺的队伍裡掉队的話，那还能剩下什麼……]
[所以说你才是个笨蛋啊。那种东西怎麼可能知道啊，理由什麼的可不是由他人施舍给你的。自己的生存意义什麼的，就由自己找出来。比如说啊，你不是有着其他傢伙都无法企及的程度的优秀的神魔法么。有那个的話，就算没了後盾不也算是一样财产么。只是将他人所说的当做理由，然後以此来当做动力来行动，在这个世界裡简直太天真了。那你又是为了什麼，才拼命地用自己的双手将那份力量掌握在手中的呢。如果说你是想成为王的話，那也无所谓。但是，那真的是你心裡所渴望的吗。就算过後後悔了，我也不会帮你的哟。一定会笑你活该的]
对於随性放話的玛丽薇尔，米蕾亚除了摆出自嘲的微笑以外什麼都没说。
虽然觉得稍微有点说得太过了，但玛丽薇尔对於说出的真心話却是一点都没後悔。
作为自己姐姐的米蕾亚，在兄弟之中有着与世无争的性格。无法违逆强者，没有自己的主张，胆怯。
但是，玛丽薇尔却知道作为那个的弥补却有着过剩的正经和温柔。虽然从来没有坦率说过，但在兄弟姐妹里玛丽薇尔却最喜欢她。
因此，这次的事件才觉得即使帮她一把也无所谓。同时觉得在她掉队的时候应该拉她一把。
米蕾亚，并没有扑通一下盘腿坐到王座上的豪胆。玛丽薇尔觉得她更适合在小小的教会裡照顾孩子。
只是，玛丽薇尔并没有温柔到会将人的生存方式推给她的程度。由他人所安排好的人生的道路，虽然轻鬆但之後留下遗憾了也是没有回头路了的。
不用轻鬆，不停烦恼，然後再做出选择。那才应该是，米蕾亚不再作为王族的棋子，而是作为一个人类而活着的意义。
虽然玛丽薇尔嘴上不提，并把头扭向一边，但意识却飘向了米蕾亚说出的話裡。
[要坚强起来，很困难啊……我，不知对你那种坚强的人生羡慕了多少次]
[只要行动起来不就行了嘛。話说在前头，我是真的很强哦。因为我对自己的强大很有自信]
[呵呵，真不愧是你啊。玛丽薇尔，你到底为什麼，会那麼强呢？]
[那又有……]
話说到一半，玛丽薇尔就停下了嘴。
为什麼，自己会追求强大呢。自幼开始磨练剑法，一味地渴望变强，到底是为了什麼呢。
直到懂事后，最後在骑士团的锻炼所才发觉。發现磨练剑的技术，就是为了看到遥远的高处。
但是，那个根源到底是在哪呢。在小的时候，自己确实是看到过的景色，那正是自己所追求的。随着时间流逝渐渐淡忘，现在已经回想不起来了的记忆。
轻叹口气，玛丽薇尔摇摇头。想不起来，但总有一天会想起来的吧。自己到底，是为了谁，为了什麼而握起剑的。
玛丽薇尔心不在焉地思考着，但却强行停下了思考。是因为听到了米蕾亚抛出的质问，所以才能做到的吧。
[说起来，利安先生是你的意中人么？玛丽薇尔跑出城裡的理由，难道是私奔？]
[很好，在利安面前把你扒光吧。被什麼样都不会帮你的，尽量努力吧，各种方面都是]
[骗，骗人的骗人的吧米库小姐只是开玩笑的啦得意忘形了非常抱歉！]
骚动之夜变明亮后，三人再次骑马向东北前进，最後终於成功赶完路程了。
好不容易走到目的地，王族试練的洞窟的时候，已经是第二天的傍晚时刻了。

