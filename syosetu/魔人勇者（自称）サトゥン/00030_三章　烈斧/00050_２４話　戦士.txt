第二天早上，利安仰望放晴了的天空，安心地呼了口气。
昨天就像古连弗德预测的那样，是个寒风吹个不停，大雪一直在下的恶劣天气，但今天却像是没發生过一般风和日丽。

吃完早餐，一行人为了向冰蛇所在地出發做着準备，但他们的视线却频频转向撒东那边。
不管是利安，还是玛丽薇尔，还是米蕾亚。个个人都一次次地看向撒东，準备速度非常缓慢。
为何大家都在看着撒东呢。理由很简单。因为撒东，居然安静下来了。
直到昨晚晚饭前，他都还在一边唠唠叨叨个不完。现在却好像是在思考着什麼那样，闭着眼睛架着手。
沉默伫立着的撒东，不管谁来都会觉得是个名副其实的美青年。脸庞的每个部位都有着非人般的美貌，锐利的双眼高挺的鼻梁，就算是王族在他的美貌前也要逊色一筹。
可惜，美中不足的是，平时他总是每时每刻都在暴走，将那幻想彻底打碎。
一开口就大笑，说的东西都是勇者勇者什麼的。那太过破天荒的行径，将他从美青年的範围裡踢飞到遥远的天边去了。
因此，此时的形象骗不过作为他的【被害者】的她们。看着沉默的撒东，玛丽薇尔只觉得【很恶心】，米蕾亚则是在想【是不是吃坏肚子了】。

而唯一真的担心他的利安，问了撒东發生什麼事後，撒东却只回了一句【没问题】。

[那麼，古连弗德桑，承蒙您的关照了]
[啊啊，虽然这話不该由输给了你们的我说……但还是小心点。冰蛇……那傢伙，非常强]
[哼，知道了啦。我们会将你的悔恨解决掉的，请耐心等待]
对於一行人的致谢，古连弗德轻轻点头接受了。
玛丽薇尔和米蕾亚骑上了马，出發时间逐渐迫近。古连弗德为他们送行。
心中萌生的言语想拼命抖落阻止它的迷雾，但古连弗德却拼命地用理性将自己这股衝动扼杀掉为他们送行。
撒东赢了他，而他则败给了撒东。自己没有理由阻止比自己还强的人去打倒冰蛇。
所以他对自己说这样就好。肯定，能将那些被怪物杀死的人们超度的吧，能将他从这咒缚裡解放出来的吧。
不久，到了出發的时刻。玛丽薇尔开始策马前行，但那步伐却很快就停下了。
因为利安并没有跟上她们的行军。
利安停下了脚步，转向古连弗德。
紧握着枪，仿佛想要传达什麼，但又没传达出去。古连弗德问他怎麼了後，利安像是下定决心般开口说道。
[——我，觉得古连弗德桑就是英雄。就像撒东大人对我而言那样，被古连弗德桑救助过的贝尔德鲁镇上的人们，绝对是很感谢古连弗德桑的]
[利安……]
[假如，撒东大人现在和古连弗德桑一样深陷痛苦中的話，我觉得我心裡肯定会满是悲哀和後悔的。拯救了我们的英雄，一直深陷於对自己过错的自责和後悔中，那得到了救助的我们该怎麼办才好啊。觉得我任性也好。觉得随便也罢。可是，换做我是被救了的镇民的話——会希望，你能作为英雄继续下去。希望这个不顾自己性命救了镇民的命的人，能以冰蛇为对手战鬥到最後，并倖存下来，作为英雄能被其他人所赞颂]

利安说完，就背向古连弗德朝玛丽薇尔她们那边走去。
古连弗德呆呆地看着已经看不见了的利安他的背影，心中像是被他的話深深地刺穿了。
他，有考虑过被救了的人们的心情吗。有考虑过被拯救了性命的人们的事情吗。
在那之後，有接触过被拯救了性命的人吗。没有，为什麼呢。很简单，是因为自己自顾自地逃走了。
害怕被救下的人会跟王都的人一样，对他进行漫天咒骂。害怕他们指责自己下了错误的判断害了他们家人的生命。
如果听到了那些漫骂的話，就再也无法振作起来了。如果听到了那些指责的話，肯定会迷失掉在这世上或者的意义的。
所以他断定。残活的人们的愿望，就是对冰蛇的復仇。为了让自己安心，想一个理由出来是很轻鬆的。
在冰蛇附近生活了十多年，也不过是想要一个免罪符罢了。希望别再出现更多的牺牲者，这样自己就能轻鬆了。

利安的話，让人心痛。
他说，他被撒东拯救过。假如撒东陷入了和自己一样的境遇的話，他就会像話里所说的那麼想。
那一天，自己没有反驳王族们的任何一句話或是决定。那是作为臣下的高洁的同时，也只不过是一种逃避罢了。
被流放掉一事，也是对接触被救之人的想法的一种逃避。现在的話，就能明白应该怎麼做了。
自己，应该去见他们的。应该直接去听一听，存活下来的他们的話的。
无论是什麼样的話语，都要接受下来。因为那是唯一的，作为在那天对阵冰蛇却什麼事都没做到的英雄所能做的最後的行动。
能声讨自己的人，既不是王，也不是其他民众，更不是自身。最应当声讨自己的，除了那些被救下的人们以外没有任何人有那个资格。
然後，假设，正如利安所说的那样，他们还把自己当做英雄的話，那麼——

[——现在才想到吗。太迟了，一切都太晚了。我对於英雄一词而言，太过愚蠢了]

後悔向古连弗德袭来。事到如今，只能自嘲来不及了的他已经什麼都做不了了。
事到如今自己还能做啥。在这十年间，一直背叛着存活下来的他们的自己，到底会遭到什麼样的报应呢。
一直天真的认为都是自己的错，一直以为自己现在做的事是每个人都希望的这样逃避着，结果就落得现在这般模样。
人类是随着岁数增长而越来越难自立的生物。让他像利安和玛丽薇尔那样，勇往直前的活着，太迟了。
大人，是没有理由的話就不会行动的。那是在巧妙周旋，和狡猾的感情下的行动。对不考虑後果，鲁莽行事感到害怕。
想让古连弗德动起来，後悔的心却太过沉重。想让他动起来，只能从他背後给他一脚。如果不是用将他踢飞滚倒在地的力道的話，背负着悲哀过去的他一步都迈不出去。
那麼，要怎麼样才能让他动呢。连利安的話都不为所动，要怎样才能迈出去——那很简单，只要有一个真能做到【那个】的胡来的破天荒存在就行啦。

[——哼！]
[哇！？]

突然背後传来一阵强烈的冲击，古连弗德就那样倒向地面。
不，没那麼简单。冲击太强了，那是能让他撞到树上为止都滚个不停的强烈的一击。
明明没感觉到气息，到底是谁向古连弗德放出那麼强的一击呢。那样的人物，只有一个。
那个青年——勇者撒东一副不高兴的表情，蔑视着古连弗德说。

[怎麼，这不是还能动嘛。因为磨磨蹭蹭的停在那个地方，还以为脚已经石化了呢]
[是撒东啊……冰蛇那边怎麼了，别说已经打倒了啊]
[哪裡，只是有点东西忘了拿了。马上就回去了。说到忘了的东西，就稍微教一下你这傢伙，英雄到底为何物吧]

撒东闭上一只眼，竖起食指，掌心面向古连弗德淡淡地说。

[其一，所谓英雄是为了守护他人而竭尽全力之人]

古连弗德呆呆地让撒东的話流入耳中，撒东逐渐增加竖起的手指继续说。

[其一，所谓英雄是成为他人的希望之人。
其一，所谓英雄是无论何种困境都能突破之人。
其一，所谓英雄是不会骄傲努力钻研，为了他人而行使那份力量之人。
其一，所谓英雄是不会对自己服输不会灰心绝不放弃之人。]

五指竖起，撒东向着面前的古连弗德，从自己体内放出魔力。
古连弗德连呼吸都忘了，只是忘我地沉醉於那幅景象。以撒东为中心，脚下展开了黑色的魔法阵，光开始从阵中奔流而出。
随着强大的魔力的萌芽，从撒东口中编织出只有魔之人才能够咏唱出来的暗之契约的咒文。
不久，魔法阵从中间开始崩裂，从无盡的黑暗空间裡，显现出了一柄巨大的斧头。
那是把长度远超撒东身高的巨大双刃斧，那把充满暗黑色彩的斧头，缓缓地向古连弗德抛去。
古连弗德小心翼翼地拿起斧头，却对那斧头重量之轻——不，幾乎感觉不到的重量惊讶不已。
然後，在握住斧头的同时，从身体裡爆出了一股力量。古连弗德发觉，那是自己身体裡的鬥气。

对於因斧中满溢洒落的力之奔流而跪倒在地无法言语的他，撒东向他的背後方向迈出脚步，并淡淡地说。
[和现在的你比起来，利安和玛丽薇尔可是在遥远的彼方成为了英雄哦？
利安为了从魔兽手中守护村子，就算会丢掉性命也要站在魔兽的跟前。
玛丽薇尔为了守护家人，即使面对无法战胜的强大敌人也要砍上一刀。
我是很爱护英雄。但强者并不代表就是英雄，由他人的称讚所构筑的地位之人也不意味着就是英雄。
——只需要简简单单的五点，不管是谁说了什麼都不曾改变心意笔直地贯彻此信念之人。其生存方式就会使人着迷，那类人不正是人类称之为英雄的存在吗]
[英雄……]
[我相信利安没有看走眼。
就像我从利安和玛丽薇尔身上看到了那样，我看到的那个也是你英雄的光芒吧。
——别让我等太久哦？我可不像利安，会慢吞吞的等着你呐。
在我改变心意之前，你能够得出自己的答案就行。在这十年间，你是怎麼看待斧头的，又是为了什麼挥舞着它的？
你不断钻研的那份力量，到底是为了谁而存在？我相信，你所得出的答案，和我所期待的答案会是一样的。]

留下那些話，撒东如字面意思般从他的眼前消失了。
撒东宛如幻觉般消失了，但他给予古连弗德的斧头却确确实实在手中闪耀着黑色的光芒。
握紧了斧头，古连弗德闭上眼喃喃道。

[我的力量……是为了谁，而用吗]

古连弗德缓缓站起来，将斧头背在身後，跑了起来。他要前往的地方，只有一个。
不过，与之前相比他就像是变了一个人一般。如果梅娅看到了他的脸，肯定会表现得欣喜若狂吧。

——那是，确确实实身经百战过的【战士】的脸。

正想着突然消失了，却又突然出现了。
玛丽薇尔对突然消失又出现的撒东扔出了【别擅自行动】【到哪裡去之前先说声再去】之类的说教，但撒东完全就是当做耳边风。
只是，利安却从撒东身上感到了一丝微妙的变化。
那是和往常的撒东……不，虽然沉默的样子也很异於往常，但总感觉和平常的撒东有点不一样。
他在大体上感觉和平常没啥区别，脸色也很好。可是，利安却若有所思地说了一句話。

[撒东大人，是不是身体哪裡不太舒服啊……？]
[应该不是，虽然怎麼看都觉得和平常的撒东大人不一样……]
[虽然平常的样子是很恶心，但现在这样真不像你的风格，快变回原来的藻类（调调）啦]（PS：原文ノリ，有藻类和飘飘然、节奏的意思，这裡应该是作者弄的日语双关）

利安的話，在传到米蕾亚那边後，又变回了往常的对話。
撒东發现利安一直惴惴不安地看着他，向利安看了一眼，然後刷地摸摸利安的头。
撒东摸着利安的头，用认真的语气——自爆了。（PS：自己爆料的意思，别误会）

[其实，从昨晚开始一直在烦恼……但就在刚才得出结论了]
[在烦恼着，的吗？]
[嗯，那个烦恼就是……到底要用哪个方法将冰蛇打倒，才能让我的名气流传到後世！然後我终於决定了！那条冰蛇，就用吾身体裡火热翻腾的强大魔力将其烧掉吧！我在此向你们宣言！那条冰蛇什麼的，就让吾用魔人界独一无二的业火【勇者暗黑炼狱炎】将其一根骨头都不剩地蒸发掉吧！唔哈哈哈哈哈！]
[明明是勇者却用暗黑什麼的呢……]
[还想着直到刚才为止一直安安静静的在幹嘛，原来和平常一样呢……一直闭嘴不言的样子还好点]
[呼哈哈！废話就到此结束了！看来粉饰吾的荣光舞台的勇者之敌要出来啦！]

撒东話没说完，玛丽薇尔和米蕾亚就立刻下马进入战鬥状态了。
利安比谁都快地握好枪，紧盯着离他们数百米远的前方。
在他的视线前方，是一片宽阔的空旷场所。不对，那并不是人工制造出来的东西。
而是在这数十年间，由於某样巨大的东西无数次地爬过，变成了寸草不生的场所。

没错，在那裡已经有先来的了。
那个先到的，發现了利安他们一群人，慢慢地抬起趴在地上的头，發出了响彻山脉的高鸣。
雪白长长的身体，覆满了冰之鳞片，赤红的瞳孔仿佛追寻着猎物般乱动。
身体远超镇上两层建筑的高度，像是大马路般的大小。然後身长一眼望不到边。

在过去，大约十年前将一整个城镇破坏殆尽的怪物——冰蛇雷启迪西斯。

在那样巨大的躯体面前，一行人不管是谁都言语尽失。
这就是，将那座城镇恐怖地埋没殆尽的怪物吗。这就是，那位英雄弄成那样衣衫褴褛的怪物吗。
各种各样的思绪在脑内徘徊的时候，利安和玛丽薇尔下定决心冲向怪物。要说为何，因为冰蛇比他们还快地盯上了他们四人开始动了起来。
为了不让唯一的治疗人员米蕾亚被打倒，前卫的两人只好前进。
撒东大叫起来给那两人下了指示。

[姆哈哈哈哈哈！离吾發动【勇者暗黑炼狱炎】还要稍微花点时间！在那之前就拜托你们两人争取下时间了！]
[说要花点时间，要多久啊！]
[不用多久啦！我想想啊，也就三十分钟吧！在那之前请务必撑住啊，唔哈哈哈哈哈哈哈哈哈！]

撒东那满意了的指示传入耳中後，利安和玛丽薇尔对视了一眼得到了一个共同的结论。
撒东指望不上，这次还是得靠我们自己想办法做点什麼。
冰蛇雷启迪西斯和勇者他们的导火索，就此揭开序幕——和变回了平常样子的，他的大笑一起。

