
“喂、我已经知道这裡有一位叫做直见真嗣的人了!快出来!!”

体感感觉现在深夜12点左右吧。
我们在马车的货架上聚在一块睡觉的地方、听到了外面怒吼的声音。
从脚步声来看、好像聚集了20名左右的人。

“主人…”

露西娅害怕的样子、轻轻的抱住了我的手。
她因为有受到虐待所造成的创伤、所以害怕夜晚的声音。
因此、无论无何也要依靠打从心底信赖的我。
总有一天也必须要独立吧、虽说是S级的冒险家、但现在是没有我的話就不行的孩子。不守护她的話不行。
我摸了感到害怕的她的头一下、一瞬间害怕的表情就消失了。
所以、对我非常的信赖的意思吧。

“放心的睡吧。现在不是小孩子该起床的时间。艾琳也是。会长不高的喔?”

我开玩笑的说道、露西娅再加上果然醒过来了的艾琳也点头了。

“了解…多亏了主人现在感到非常的安心。明明到刚刚还那麼不安的、那样恐怖的感觉现在完全感觉不到。
果然主人很了不起。

“但、但是、专程要麻烦真嗣大人动手也很难过。果然还是由我来代替……”
“小孩子不必做勉强的事、那种事是身为大人的我的职责。”
“茜也是大人~。想和真嗣先生一起战鬥~、加油囉~”
“也请务必让我陪你去、直见大人。”

果然起来的茜和舒碧儿也跟着说。
总是悠閒的茜和、冷静的舒碧儿、不知道为什麼听到可以和我一起战鬥显得战意特别高昂。
嗯、为什麼呢?
算了、在和龙战鬥之前能够看到她们的实力也不坏、吧?
当然、只有我一个人也没关係、那麼今後、我会在一瞬间把所有的敌人都解决掉。
要是那样的話、对少女们的成长就行不通了吧。
我已经是最强了所以不需要修行、但是少女们还有许多成长的空间。
要是稍微能够接近做为师傅的我、在这样的实战和我一起战鬥的話、对她们来说也是非常好的训练吧。
再加上、有我在的話、不会让她们發生万一所以很安心。
因此、我和茜还有舒碧儿3人、把马车的遮蔽物用猛烈的气势翻开後、站在了地上。
对於我们威风的登场、先前發出怒吼的穿着骑士打扮的年轻男子”呜哇!”發出难听的声音屁股着地摔倒了。
跟我们威风凛凛的姿势相反、愚蠢的就像是要我们嘲笑似的样子。留有贵族样式的长长的金髮。

“呼呼呼、在做什麼阿、你是?不要突然就让我笑好不好?哈哈哈”

我边低头看那个骑士、边拼命的忍受着笑意。
但是、不知道为什麼那个骑士看到我、脸突然变红开始生气了起来。

“你、你这家夥!!别以为侮辱了身为罗德鲁公爵家的长男、
骑士团的杜克大人我能就那麼算了吗?这个区区的平民!!!”

我把那样的怒骂声当作耳边风。

“所以、你们到底是来这裡做什麼的?我们可是在睡觉当中喔?不觉得很没常识吗?”

就那样子询问道。

“你、你这家夥听到我说的了吗!!”

但是、因为听到了这样的声音、我再次俯视着跌坐在地的男人。

“……呼、呼呼呼、喂、赶快站起来。难道你是为了让我笑而被生下来的吗?不、说不定真是这样呢。
那样也有价值喔。做为小丑呢。呼呼。呼。”

在那样的話语之下、名叫杜克的男人像是要从嘴裡流出血似的恨得咬牙切齿、而且脸也变得暗黑色。
但是、果然还是精不起我持续的嘲弄似的、一边顶着因受屈辱而扭曲的脸总算站了起来。
嘛、能够站起来也是因为被同伴扶起、动作到底是有多迟钝啊。
那样子又为了引诱我发笑、不知不觉脸颊就浮现出了微笑。

“哈哈哈、你真的是逗我笑的天才阿!请别这样阿、在这样的深夜裡。哈啊、肚子好痛。
我喘着气、好久没有这种上气不接下气的感觉了。
呼~这是个可怕的敌人。肯定是为了让我笑死才来的没错。

“从刚才开始就一下子变白、一下子变红、现在又变黑色、像是腐烂的西红柿一样、总觉得真是很有趣的脸阿。”
“那个、直见大人、差不多该听他说話了比较……。总觉得对方太可怜了。”
“那麼说也是呢。喂、你说你叫杜克?你到底是来做什麼的?在这个地方说明一下吧。

我这样命令道後、杜克在一次的用怨恨的眼神瞪着我。
用着像是能咒杀人似的、充满漆黑感情的眼神。
那麼、是为什麼呢?我愣的歪着头、我难道做了什麼坏事吗?
杜克看着我那样的反应、又开始發出嘎吱嘎吱的声音咬牙切齿了起来。但是、这一次他一边把脸变得通红、
一边發出奇怪的声音、杜克就开始说明了。

“能够得意的也就只有现在而以平名们!!我们是王国骑士团!!是受到国王敕命的人!!
如国反抗敕命的話就会立即处死!!然後、你现在的态度很明显的是愚弄受到敕命的我们的行动!!
不、并不只有那裡!!对身为大贵族嫡子的我做出无礼至极的举动、以经很难原谅了!!
完全不用带到伊西吉玛那裡!!就在这被砍头吧!!!!!!!!!”

杜克那麼说着、一边摆动着变得乱糟糟的散开的金髮、一边拔出了剑。
呜哇、总觉得刚刚跌坐在地的时候泥就飞溅到了头上跟脸上、变得非常的脏。
不想靠太近阿~~~……。

“要真嗣先生拔剑什麼的、就像是反抗神明一样喔~?我觉得还是算了比较好喔~。
任何的事都不自量立的話~”
“就是那样、而且直见大人是唯一一位冒险者协会联盟全面协助的人。

那个可不是像你这样一个贵族可以搭話的存在。会从联盟发表正式抗议喔?、
我可以理解她们的話。

“杜克、就像你听到的一样。像你这样的垃圾和我说話是不可能的。赶快回去比较好。
啊!还有如我伊西吉玛要见我的話、请转告他叫他自己来见我。虽然是不会直接来见我的胆小鬼、
姑且算是曾经的老朋友。如果是自己来的話、姑且是会见一面的。

“哇~、真嗣先生还真是温柔呢~。即使是像杜克这样的人也照样给予同情呢~。

“嘛、如果让直见大人来的話肯定连塞牙缝的程度都不够呢。那麼、杜克、就像你听到的一样。
感谢直见大人的温柔、赶快夹着尾巴回去吧。大致上现在也以经是深夜了呢?
如果要晋见直见大人的話请再多少有一些常识之後再过来。”
“你、你这家夥!!!!!!!!!!!!!!到底要愚弄我到什麼时候才甘愿啊!!!!!!!!!!!!!!”

那样叫着的杜克朝我这裡砍了过来。
不、并不只是他、包围的同夥们也一起。
但是、就算来幾个人……不、来幾万人和这个杜克跟被称为是精锐的骑士们进攻过来也是一样的。
在我看来也是像慢动作回放一样、在这个瞬间也能使对方再起不能1万次。
说白了点这哪裡算得上是準备活动、连攻击也称不上。
但是、这次的目的是别的。对、也就是同伴们的成长。虽然我已经到达上限了、
但是她们仍然可以通过实战来成长。就算稍微也好为了追上我而必须要努力。
另外、也要充分的理解她们的力量。对於我的强大她们到底跟上了多少呢?
因此、我向着茜和舒碧儿看去。於是、她们好像一瞬间察觉到了我的意图、一边微笑、
一边小声的说着请交给我们。

“偶尔呢~、茜也不能做不像精灵神的事呢~、来吧、安魂曲——水魂
(译：某大佬所想的超帅的咒文：进入永恒之黄昏吧，终末的安魂曲！)
茜在那样咏唱着的同时、跑出了5只像是魂魄的水灵像骑士们袭击过去。
原来如此、好像是使役了从天界召唤的水之魂的技能。虽然由我来的話是一瞬间就能解决的强度、
但是如果是以骑士团为对手的話能够跟10000个人当对手。
现在各个水灵也以无咏唱的方法施放出强力的咒文、把5人的骑士团吹飞了将近50公尺、
总算是保留了一命昏了过去。
……茜的场合的話。肯定、因为对方很多人要自己来很麻烦、所以才选择了召唤魔法吧。
非常有茜的风格、因为很懒散所以是很合理的战术。
嘛、就给她算合格吧。
另一方面舒碧儿用一把小刀、和加上杜克共5个人的骑士对抗。

“笨蛋!!凭那种小刀!!!”

真是的、笨蛋的是你喔。

“…………啊?”
“喂…喂、你的剑掉了喔?”
“不…不、你才是。”
“赶、赶快捡……起!?手、手动不了。”
“为、为什麼啊!!为什麼手动不了了!?!?!?”

呼、连那种程度的动作都看不清楚吗?
已经不在战鬥的棋盘上了。
更不用说和我战鬥了、也只有笑了。
……啊、不。确实是从一开始就在笑了。

“没有被杀就该感谢了。因为是用涂了毒药的小刀刺进手腕的肌腱、所以如果不赶快使用回復魔法治疗的話、
会变得再也动不了吧。”

是的、她用非常惊人的速度和准确度挥舞着小刀。在我看来是很缓慢的、
但是对骑士团来说似乎并不是那麼一回事。就在没有感觉到痛觉之下、
手腕的肌腱被破坏、变得连剑也握不住了。
嘛、这裡也给她合格吧。

“呼、已经可以了吧。对上茜还有舒碧儿就变成这副模样、根本没办法当我的对手。
刚刚也说过今後要更加有自知之明、好好的活下去就好了。”

我那样说完之後、杜克露出不甘心的表情而发抖。
但是我、

”那的手腕、差不多再也不能动了喔?”

那样说着、终於脸色变的苍白就快要哭了出来、

“可、可恶~~~~~~~~~~给我记住~~~~~~~~~~~”

一边按住手腕、一边尖叫着跑走了。
其他的骑士们也一边叫起昏迷的团员们一边追在杜克的後面逃走了。

“呼、还真是可怜的家夥呢。嘛、如果说是来让我笑的話、做为小丑来说是及格的。"
我就在嘟哝那样子的事时、茜和舒碧儿好像在期待什麼似的往这裡靠了过来。什麼什麼?

“今天茜、很努力了吧~?总是都只有露西娅和艾琳实在是太狡猾了~”
“如果受到相同的对待我、我也会很高兴、吧。”

这麼说的两个人就把头伸了出来。

“那个~、摸摸就可以了吧?”
“没错” ”当然!”
“就算要其他的奖励也…”
“不!摸摸就好!!!!!” “就好~”

真是的、被我这种人摸摸有什麼好高兴的。
我边歪着头边摸着两人的头。
两个人就像是在幸福的最高峰一样表情逐渐融化。
真是的、为什麼会那麼幸福呢?

