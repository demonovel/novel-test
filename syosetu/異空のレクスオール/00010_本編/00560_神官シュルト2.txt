
於是要等人回到了黑犬之尻尾亭，不过艾丽莎依然还没有回来。
要和海因兹的房间……现在已经是要一个人的房间裡三人都聚集在一起，要和艾莉婕并排坐在一侧的床上，而修路特则坐在了另一边。

“以艾丽莎的脚程的话，应该差不多该回来了啊……”
“要大人您把艾丽莎想成什麼了……？”

在要的心裡，艾丽莎可以说是有着相当万能的印象，不过当然，她并没有超人到那种地步。
把街道的大门全部转一遍可不是简单的事情，就算是巡回马车都不可能在这点时间内绕上一圈。
如果真能遇上马车的话说不定能多少缩减点时间，但就算这样也会花费相当的时间吧。
要还想更加超过的话……那才真是，只能依靠魔法了。

“嘛，艾丽莎的事就算了。要大人，说到底那边的神官究竟是哪裡的哪位啊？”
“啊啊，那个……”
“鄙人侍奉雷库斯欧鲁神，名为修鲁特。这次，是由要君接下了我提出的委托”

委托，听到这个词，艾莉婕“难道说”向要看去。
冒险者公会、委托、以及神官。能推导出的答案，只能是一个。

“您把那个护卫委托……接受了吗？”
“嗯，是的。稍微有点缘由”
“缘由是……”

话到嘴边，艾莉婕注意到了“那件事”。
要特意使用“缘由”而没有明说。
要说在这裡不能明说的“理由”的话……就肯定与“无限回廊”有关。
这样的话，就不能在这裡继续深究下去了。

“……对我来说只要要大人能够安然无恙的话，那就没有问题了”
“是、是吗”
“真要说问题的话，也是那边的神官呢”
“我吗？”
“是啊，为什麼会选择要大人……相当难以理解啊”
“啊，那个啊，艾莉婕。似乎是魔力”
“要大人请闭嘴。您总不会说不了解神官选择护卫的基本方法吧？”

对来自艾莉婕的凝视，修鲁特刻意耸了耸肩。

“选择护卫时应当尽可能选择威慑力强的人，这句话吗？”

护卫的职责，不言自明，就是保护雇主。
说起来的话，就是击退盗贼和怪物……但是为击退本身而进行的“战鬥”本身，也是让雇主遭遇危险的行为。
虽然其本身是无可奈何的，但是姑且不说怪物，对盗贼来说却是有着判断“袭击对象是否有漏洞”这样的依据的。
虽然以上的话听起来似乎有华无实，但是其实裡面也确实地包含着免於盗贼袭击的办法。

“也就是“数量”和“外观”了。您真的觉得，要大人和这两点能沾上边吗？”

虽然被说得很惨，但真的要问要是不是个肌肉壮硕“怎麼看都”很强的男子的话，那也确实不是。
既没有久经沙场的战士般的外表，统帅众多同伴的领袖外表……要也完全没有那个自信。

“哈哈哈。虽然没有那些，但我比起“外表”更重视实际的实力。盗贼姑且不论，怪物可不会畏惧外表的吧？”
“这一点，我也不觉得光靠看一看就能了解的……嘛，算了。既然要大人已经接受了委托，那您就是委托主了”
“哦呀，您明明看上去并不怎麼像是冒险者，这样可以吗？”

修鲁特吃了一惊般的话语让艾莉婕无言地瞪了过去……而在要试图对这微妙的气氛做些什麼而开口前，房间的大门就伴随着巨响被打开了。

“哈……哈啊……有、有了……”
“啊、艾丽莎？”
“跑过来的吗？为什麼这麼乱来……”

虽然艾莉婕赶紧站起来朝艾丽莎走去，但从门口摇摇晃晃地走进来的艾丽莎却径直把手搭在要的双肩上……就这样，相要将他推倒一样倒在了床上。

“呜、呜哇哇！？”
“……太好了。还想着……你一个人……会去哪裡……”

对艾丽莎的话要什麼都说不出来，而赶紧想将艾丽莎拉开的艾莉婕的手也停了下来。
真的相当担心。而且，真心地被担心了。
切实地体会着这份事实，由此产生的歉意与喜悦。
感受到的柔软的触感，传过来的温暖的体温。
滚烫而慌乱的呼吸，如同感染了那份炙热一般要的脸也红了起来。

“嗨呀！”

果然实在无法忍受的艾莉婕将艾丽莎从要身上拉起，从怀中取出小瓶，将内容物倒入艾丽莎的口中。
在似乎是某种液体的那个被艾丽莎喝下後，可以看出艾丽莎的整体状况都很快地恢復了正常。

“魔力药吗。用的毫不犹豫呢”
“哼、毕竟她不恢復正常的话，对话无法进行下去啊”

对感慨着的修鲁特艾莉婕如此回復道……终於，气息完全调理好的艾丽莎，从床上猛地抬起身子。

“好了，恢復！艾莉婕，之後我会付钱的”
“没有必要。比起这个，要大人接受了那个委托哦”
“那个是……啊—”

艾丽莎这时才像刚注意到一般转向修路特，从床上站起来伸出手。

“您好，我单名艾丽莎。虽然也算不上什麼领队，但姑且这类事务上我最有经验。请多关照”
“这真是多礼了。鄙人侍奉雷库斯欧鲁神，名叫修鲁特”

修鲁特握住艾丽莎伸出的手，但是接下来的瞬间……艾丽莎微笑着说出的话语让他的表情完全冻结。

“的确是从此处直到米兹镇的护卫吧。大概三日的行程，所以算您金币二十枚。当然，对障碍危害的排除也包含在内。您意下如何？”
