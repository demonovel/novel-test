# novel

- title: 乙女ゲー世界はモブに厳しい世界です
- title_zh: 乙女遊戲世界對路人角色太嚴厲
- author: わい／三嶋　与夢
- source: http://ncode.syosetu.com/n3191eh/
- cover: https://images-na.ssl-images-amazon.com/images/I/912nndTR2SL.jpg
- publisher: syosetu
- date: 2018-11-06T00:00:00+08:00
- status: 連載

## series

- name: 連載中

## preface


```
主人公在幫一抹多推乙女遊戲，結果一不小心猝死了。之後轉生（轉生套路限定），本來可以享受一番，但好死不死轉生的那個世界卻是自己所玩的那個乙女遊戲的世界（奉行女尊男卑觀念）。  

轉生之後名為【利昂·冯·巴尔特费尔德】（是男爵和小妾的次子），而且被告知二十歲沒結婚就會成為老女人（歐巴桑）的後夫。本可以通過入學擺脫將來的命運，卻被家裡的那位（所謂的父親正妻）給安排了要娶一個大嬸（肥婆？）的命運。主人公奮起抵抗，試圖去扭轉這女尊男卑的觀念。（不過還是成功入學？），卻又被捲入由（玩家？）所引發的一系列遊戲事件當中。  

【該作看似乙女小說但實際上是男性向】  

簡評:男主脫離苦海and打臉五傻（帥哥）and無意間攻略（然鵝自己配不上）

現代日本から転生した世界は――まさかの女尊男卑の乙女ゲー世界だった!?
男爵家の三男として第二の人生を歩むことになった「リオン」だが、そこはまさかの知っている乙女ゲーの世界。
大地が空に浮かび、飛行船が空を行き交うファンタジー世界だった。
冒険者が存在し、何ともワクワクする異世界。
だが、リオンは素直に楽しめない。
それは女尊男卑が凄い世界で、貴族の男は婚活に悪戦苦闘する世界だったからだ。

外道な自称モブ男主人公が暴れ回るゲーム系異世界ファンタジー！

※GCノベルズ様より書籍化。一巻発売中！
```

## tags

- node-novel
- R15
- syosetu
- ダンジョン
- チート
- ロボット
- 一人称
- 乙女ゲーム
- 人工知能
- 冒険
- 学園
- 悪役令嬢
- 残酷な描写あり
- 男主人公
- 異世界転生
- 近代
- 飛行船
- 女尊男卑
- 

# contribute

- davidwwb123
- homtsai
- 6969letsex
- 火烤de泡泡
- 灰常瞴语
- electricfan_bk
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id: 1113080
- novel_id: n3191eh

## textlayout

- allow_lf2: true

# link

- [dip.jp](https://narou.nar.jp/search.php?text=n3191eh&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [乙女游戏世界对路人角色太严厉吧](https://tieba.baidu.com/f?kw=%E4%B9%99%E5%A5%B3%E6%B8%B8%E6%88%8F%E4%B8%96%E7%95%8C%E5%AF%B9%E8%B7%AF%E4%BA%BA%E8%A7%92%E8%89%B2%E5%A4%AA%E4%B8%A5%E5%8E%89&ie=utf-8&tp=0 "乙女游戏世界对路人角色太严厉")
- [dmzj](https://manhua.dmzj.com/yinvyouxishijieduilurenjuesehenbuyouhao/)
- [comic-walker](https://comic-walker.com/contents/detail/KDCW_FS01200541010000_68/)
- 

