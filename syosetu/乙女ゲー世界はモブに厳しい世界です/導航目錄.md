# CONTENTS

乙女ゲー世界はモブに厳しい世界です
乙女遊戲世界對路人角色太嚴厲


- [README.md](README.md) - 簡介與其他資料
- [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E4%B9%99%E5%A5%B3%E9%81%8A%E6%88%B2%E4%B8%96%E7%95%8C%E5%B0%8D%E8%B7%AF%E4%BA%BA%E8%A7%92%E8%89%B2%E5%A4%AA%E5%9A%B4%E5%8E%B2.epub) ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E4%B9%99%E5%A5%B3%E9%81%8A%E6%88%B2%E4%B8%96%E7%95%8C%E5%B0%8D%E8%B7%AF%E4%BA%BA%E8%A7%92%E8%89%B2%E5%A4%AA%E5%9A%B4%E5%8E%B2.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/tree/master)
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E4%B9%99%E5%A5%B3%E3%82%B2%E3%83%BC%E4%B8%96%E7%95%8C%E3%81%AF%E3%83%A2%E3%83%96%E3%81%AB%E5%8E%B3%E3%81%97%E3%81%84%E4%B8%96%E7%95%8C%E3%81%A7%E3%81%99.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/tree/master/lib/locales)
- [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitee.com/bluelovers/novel/blob/master/syosetu/乙女ゲー世界はモブに厳しい世界です/導航目錄.md)  ![Discord](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://discord.gg/MnXkpmX)




## [第一章](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0)

- [序](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00010_%E5%BA%8F.txt)
- [戦う理由](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00020_%E6%88%A6%E3%81%86%E7%90%86%E7%94%B1.txt)
- [失落道具](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00030_%E5%A4%B1%E8%90%BD%E9%81%93%E5%85%B7.txt)
- [入学](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00040_%E5%85%A5%E5%AD%A6.txt)
- [主人公と悪役令嬢](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00050_%E4%B8%BB%E4%BA%BA%E5%85%AC%E3%81%A8%E6%82%AA%E5%BD%B9%E4%BB%A4%E5%AC%A2.txt)
- [貴族の嗜み](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00060_%E8%B2%B4%E6%97%8F%E3%81%AE%E5%97%9C%E3%81%BF.txt)
- [真主人公](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00070_%E7%9C%9F%E4%B8%BB%E4%BA%BA%E5%85%AC.txt)
- [白い手袋](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00080_%E7%99%BD%E3%81%84%E6%89%8B%E8%A2%8B.txt)
- [決闘](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00090_%E6%B1%BA%E9%97%98.txt)
- [私怨と](00000_%E7%AC%AC%E4%B8%80%E7%AB%A0/00100_%E7%A7%81%E6%80%A8%E3%81%A8.txt)


## [第五章](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0)

- [双子の姉妹](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00250_%E5%8F%8C%E5%AD%90%E3%81%AE%E5%A7%89%E5%A6%B9.txt)
- [嘘吐き](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00260_%E5%98%98%E5%90%90%E3%81%8D.txt)
- [レリアとセルジュ](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00270_%E3%83%AC%E3%83%AA%E3%82%A2%E3%81%A8%E3%82%BB%E3%83%AB%E3%82%B8%E3%83%A5.txt)
- [新世代](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00280_%E6%96%B0%E4%B8%96%E4%BB%A3.txt)
- [別れ](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00290_%E5%88%A5%E3%82%8C.txt)
- [寄子](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00300_%E5%AF%84%E5%AD%90.txt)
- [尾聲](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00310_%E5%B0%BE%E8%81%B2.txt)
- [路庫西翁報告5](00040_%E7%AC%AC%E4%BA%94%E7%AB%A0/00320_%E8%B7%AF%E5%BA%AB%E8%A5%BF%E7%BF%81%E5%A0%B1%E5%91%8A5.txt)

