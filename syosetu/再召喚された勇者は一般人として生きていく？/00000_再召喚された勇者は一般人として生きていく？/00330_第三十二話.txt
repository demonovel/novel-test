
苍太因为花了数小时不断地读着书，所以感觉到身体变得僵硬。
放下书本，伸了懒腰後身体發出了“吧唧吧唧”的声音。

因为已经把所有的书都读过一遍了，所以将成堆的书本都放回了书架。
结束那个後便开始照着顺序眺望着其他书架。并不是有什麼特别的目的而是在找着有没有可以转换心情的有趣的书。
魔术书、历史书、故事本、学术书、词典、经济书等多种多样的标题连带书架分类且并排着。
苍太只要看到感兴趣的标题就会拿出来翻一翻地看着，直到失去兴趣後就放回去并走向下一个书架。

就顺着这样的步调做着时到了兒童书的角落。
从图画本开始、兒童向的故事本或者童話、为了学习的书等一起并排着。

从那之中取出了一本，并和其他的书一样翻一翻地读着。
但是，和至今为止的读其他书的时候不一样，没有放回书架反而是从最初开始慢慢地重新读过。
从一副懒懒和放鬆的表情变成了深刻的表情。

「这就是千年前的战役的结局吗，到底为什麼会这样……」

在那手中的书的标题是『七位勇者的传说』。那是一本记载着千年前苍太等7位勇者与魔王战鬥的故事。
读完那本书后找了类似的书并阅读，而每一本都述说着大体一样的内容。

故事的结局是苍太被魔王之血淋浴到，被施展了最後的暗魔法并失去了理智。然後，亲手杀了作为同伴的龙人、精灵以及兽人的勇者，最後受到了公主的送还魔法而回去地球。
那个公主也因为使用了送还魔法而陨落了性命。

在苍太能回想得到的範围内，在与魔王战鬥时乘着大家制造出来的破绽，给予了最後一击。
然而，那个也不是很清楚，就仿佛是在一片茫然的雾之中看得到一些些的暧昧的记忆一样。
在那场战鬥之中，每个人都尽了自己的全力然後都筋疲力尽了。

在那种状态下中了魔法的話就会堕入黑暗之中。再者说媒介是魔王的血，效果更是绝大的。普通情况下的話。

作为魔王得意的暗魔法的对策，苍太身穿了複数的光属性装备，提升魔法抗性的赋予魔法也施展了好幾层。
而且，当时是勇者的苍太受到了『光之神の加护』。

即使是以魔王的血作为媒介的暗魔法，在奄奄一息的状态下的魔王未必能使苍太堕入黑暗之中。

除此之外，苍太纠结着的问题是成为了媒介的魔王的血。
一边使用独有技能一边不断地斩向魔王，然而一次也没有血喷出来过的记忆而且剑上也没附着血。
即便说是最後一击，也不认为会有可以淋浴全身的血喷出来。

再来就是最大的矛盾，因为是好像童話一般地东西所以会流传也没办法，但是在全员不是死了就是被送还了的情况下，究竟是谁编织了这个故事呢。那就是最大的疑问，所以只要找出那是谁，或许就可以知道故事背後的真相也说不定。苍太是那样考虑着的。
勇者之中的谁存活下来了吗，亦或者是当时在场的某人呢。

「离这裡最近的是精灵族领吗……」

因为已经经歷了千年之时，所以什麼情报也得不到也说不定。
即使如此，能获得一些真相的碎片的可能性也绝不是零，那样想着的苍太把书放回去後便走向了柜台。

「欢、欢迎回来……發生什麼了吗？正摆出一副凶恶的脸喔」

回来的时候柜台只剩下了女性的管理员而已，男性职员好像是在整理着书架。

「不，没什麼。帮上许多的忙了，能返还我保证金吗？」
「好，感谢光顾。等候着您的再次到来」

苍太收取金币後，打开门并走了出去。

在伴随着日落，人流不断减少之中，苍太的脚步笔直地朝向目的地。
到达目的地的店後，打开了门。
入口处的门铃响后，店员反应了过来。

「好好，欢迎光临本店，有什麼……哦呀，这不就是索太嘛。又想来借用我店的设备了吗？」

平常时的話都是爱璐米娅来顾店的， 不过今天很稀奇的是卡雷娜在顾店。

「不了，今天是有話想和你说才来的」

虽然卡雷娜露出了惊讶的表情，但立刻就变成了笑脸。

「哈哈，想和我这个已经老了的人说話什麼的真会说令人开心的話呢。可以喔，就尽情地问吧。会关店的所以就请等一下吧」

将门的挂牌改成了已闭店、上锁、拉开了窗帘。

「撒，就去裡面的房间吧。茶之类的話就能拿出来喔。虽说爱璐米娅不在所以味道并不会好到哪裡去」

与之前的作业室不同而是被带到了别的会客室。

「就坐着等吧，我稍微去沏茶喔」

卡雷娜走出了房间，也因此苍太将房间扫视了一遍。
天花板裡设置着灯的魔道具，面对面的两张沙發，在那之间有着一个茶桌。
在房间的角落的台上摆设着花瓶并装饰着花朵使其整顿得很漂亮，打扫也做得很好。（炎阳：我艹，一个房间你也好扯…）

在眺望着房间的同时，卡雷娜也回来了。

「久等了，红茶和简单的茶点準备好咯」

有着想让人说出“这也是吗”的带有浓厚颜色的红茶和好像很好吃的饼乾在桌子上摆放着。
喝了一口红茶後，苍太喷了出来。

「嗝嚯、嗝嚯！这是什麼东西！！」（炎阳：黑暗料理）

把杯子放回桌子，拿出手帕来抹嘴。

「哦呀？有这麼难喝吗？很奇怪呢，个人认为泡的方式和爱璐米娅没什麼不同呐」

卡雷娜用很凉爽的表情喝着红茶。
作为改口而吃了的饼乾，与至今为止吃过的相比也算是上位好品的味道。

「这反差究竟是……」
「啊，那个饼乾是爱璐米娅事先烤好的喔。虽然让我来说也有点不适合，但那孩子一定能成为好妻子喔」

组起手臂，嗯嗯地点头。

苍太从单肩包裡取出水来饮用。

「那麼，我想要开始提问了，可以吗？」
「哼？嗯，说的也是呢……然後，到底是想问什麼？又是关於鍊金术的事吗？抑或是关於爱璐米娅的事吗？」

苍太摇了摇头。

「不管哪个都不是，話说想聊关於爱璐米娅的事也只有你而已吧……我想问的是，关於你的国家，精灵族之国的事」

卡雷娜停下了喝着红茶的手，就那样把杯子放回了桌子上。

「……到底想打听那个国家的什麼事情呢？」

表情变得有点严肃。

「并不是想问什麼複杂的问题，你对於那国家的现在的印象和千年前的战役以後有什麼巨大的变化吗。我就是想问那个」

-完-

