# CONTENTS

變成了美少女、但也成了網游廢人。
美少女になったけど、ネトゲ廃人やってます。
变成了美少女但也成了网游废人


- [README.md](README.md) - 簡介與其他資料
- [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E8%AE%8A%E6%88%90%E4%BA%86%E7%BE%8E%E5%B0%91%E5%A5%B3%E3%80%81%E4%BD%86%E4%B9%9F%E6%88%90%E4%BA%86%E7%B6%B2%E6%B8%B8%E5%BB%A2%E4%BA%BA%E3%80%82.epub) ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E8%AE%8A%E6%88%90%E4%BA%86%E7%BE%8E%E5%B0%91%E5%A5%B3%E3%80%81%E4%BD%86%E4%B9%9F%E6%88%90%E4%BA%86%E7%B6%B2%E6%B8%B8%E5%BB%A2%E4%BA%BA%E3%80%82.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%AE%8A%E6%88%90%E4%BA%86%E7%BE%8E%E5%B0%91%E5%A5%B3%E3%80%81%E4%BD%86%E4%B9%9F%E6%88%90%E4%BA%86%E7%B6%B2%E6%B8%B8%E5%BB%A2%E4%BA%BA%E3%80%82.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
- [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitee.com/bluelovers/novel/tree/master/syosetu/變成了美少女、但也成了網游廢人。/導航目錄.md)  ![Discord](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://discord.gg/MnXkpmX)




## [null](00000_null)

- [1話　糞告白しました。](00000_null/00010_1%E8%A9%B1%E3%80%80%E7%B3%9E%E5%91%8A%E7%99%BD%E3%81%97%E3%81%BE%E3%81%97%E3%81%9F%E3%80%82.txt)
- [2話　Girls・Online](00000_null/00020_2%E8%A9%B1%E3%80%80Girls%E3%83%BBOnline.txt)
- [3話　傭兵たちの世界へ](00000_null/00030_3%E8%A9%B1%E3%80%80%E5%82%AD%E5%85%B5%E3%81%9F%E3%81%A1%E3%81%AE%E4%B8%96%E7%95%8C%E3%81%B8.txt)
- [4話　錬金術　★](00000_null/00040_4%E8%A9%B1%E3%80%80%E9%8C%AC%E9%87%91%E8%A1%93%E3%80%80%E2%98%85.txt)
- [5話　姉](00000_null/00050_5%E8%A9%B1%E3%80%80%E5%A7%89.txt)
- [6話　史莱姆と最弱な俺](00000_null/00060_6%E8%A9%B1%E3%80%80%E5%8F%B2%E8%8E%B1%E5%A7%86%E3%81%A8%E6%9C%80%E5%BC%B1%E3%81%AA%E4%BF%BA.txt)
- [7話　來吧來吧合成](00000_null/00070_7%E8%A9%B1%E3%80%80%E4%BE%86%E5%90%A7%E4%BE%86%E5%90%A7%E5%90%88%E6%88%90.txt)
- [8話　不眠魔導師](00000_null/00080_8%E8%A9%B1%E3%80%80%E4%B8%8D%E7%9C%A0%E9%AD%94%E5%B0%8E%E5%B8%AB.txt)
- [9話　ミソラの森](00000_null/00090_9%E8%A9%B1%E3%80%80%E3%83%9F%E3%82%BD%E3%83%A9%E3%81%AE%E6%A3%AE.txt)

