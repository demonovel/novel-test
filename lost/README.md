# toc

## 只是被勇者消滅的簡單工作

- link: [只是被勇者消滅的簡單工作](%E5%8F%AA%E6%98%AF%E8%A2%AB%E5%8B%87%E8%80%85%E6%B6%88%E6%BB%85%E7%9A%84%E7%B0%A1%E5%96%AE%E5%B7%A5%E4%BD%9C/)
- link_output: [只是被勇者消滅的簡單工作](../lost_out/%E5%8F%AA%E6%98%AF%E8%A2%AB%E5%8B%87%E8%80%85%E6%B6%88%E6%BB%85%E7%9A%84%E7%B0%A1%E5%96%AE%E5%B7%A5%E4%BD%9C/)
- tags: 天野 ハザマ , node-novel

### titles

- 只是被勇者消滅的簡單工作
- 勇者に滅ぼされるだけの簡単なお仕事です
