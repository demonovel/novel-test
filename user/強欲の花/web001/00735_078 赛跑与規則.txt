
自从杰库鲁出發已经过去一个月了。我们现在正用计划的幾倍的速度前进着。
也就是说，多亏了诺瓦卢体力充足，没必要在休息上浪费时间，除了深夜用来休息，可以做到高效率的移动。把马车整备好，我拉着马车当做训练，於是2个人和一匹马无常识地在路上奔跑者着。

顺便说下，我们正在比赛谁先到达下一个城镇附近的池塘。当然我和诺瓦卢是很轻鬆的，甚至乔兹也保持着速度紧紧的跟着。
我们在郁郁葱葱的森林中奔跑者，一边回避树枝和树根，一边选择大池塘比较坚硬的地方。
现在离得有点远，第一位是诺瓦卢，我和乔兹被甩在後面，因为还在【索敌】的範围内，所以没问题。但真不愧是四足动物，在有障碍物的情况下一点一点拉开和我们的距离。
“那个......全部的理，领域内的水之精灵【水刃 水刻刀】。（我脑补不出来...这句话别在意我随便打的）”
这是我的原创技能，排除障碍物。好像被说了好狡猾，但是只指定了禁止妨碍。就是说，除了那个都可以做。
要是不这样做逆转诺瓦卢的机会就没有了。
与诺瓦卢的距离因为排除障碍物变得不再遥远了，但是距离貌似没有缩小太多，可以说诺瓦卢幾乎没有减速。应该是因为至今一直生活在森林裡吧......
原来这样的森林在野生诺瓦卢眼中算不上障碍物。
诺瓦卢不攻击而是使用幻影魔法灵活的奔跑。
我谨慎的跑着，躲避障碍物，或用魔法排除，你点点接近诺瓦卢，如果没有障碍物限制速度的话，结果应该会有变化，但这次的规则，对於人类很不利。因为那裡是....
不就出了森林，眼前出现的是很大的池塘。到对面的直线距离是500米吗？诺瓦卢毫不犹豫地跳进池塘向对岸游去。而且是用最快的速度游着泳。那傢伙一定认为自己赢定了吧。
“太甜了（天真）”
“水面行走！（轻功水上漂...）”
抵达池塘的我在水面奔跑，对意外的情况，诺瓦卢貌似也感到吃惊了。
这样看来只有乔兹看上去不公平了。（就是说森林是诺瓦卢主场，池塘是......你不说我都忘了乔兹了→_→）
最後会变成这样，也是在预测内。因为人类奔跑不可能输给游泳。
我不断逼近诺瓦卢，就在我确信在终点能超过诺瓦卢的瞬间。
在我眼前發生了小规模的爆炸，以及以猛烈的速度冲上对岸的诺瓦卢。
“什...！？”
哎呀，确实没指定不能这样做。（就是诺瓦卢用水花妨碍男主...）
最後我以些微的差距输给了诺瓦卢，一边抚摸着一脸得意的诺瓦卢的头，一边等乔兹到达。
是否该说它最後做的算是妨碍呢？
我用肘戳着诺瓦卢的身体，而诺瓦卢用鼻子發出布布噜的声音，微笑着看着我。（为啥你能看懂诺瓦卢的表情啊..）

【役使】说了“不进行那个程度的妨碍”，实际存在漏洞，看来是不受伤程度理解了。确实我没受到一点损伤，但是诺瓦卢的生命值减少了100左右。把豁出去的攻击用於普通的遊戏就会这样。
不久乔兹也到了。首先把湿透的衣服乾燥。
“哈，和预料的一样。”
乔兹即使这样也和平常一样。
“技能和状态都是这样，现实还真是残酷啊。中途听见爆炸声，發生了什麼？”
对於乔兹的提问，诺瓦卢斜着眼。（→_→）这傢伙也变得很有人性了。
“啊，没什麼，正在考虑乔兹的惩罚遊戏啊。”
“好像听到了关乎生命的话......”
“什麼，我也不是什麼魔鬼，姑且在下一个城镇盘查时句尾加上喵。”
开始了平时同时进行训练和遊戏的状况。认为比起什麼都不做，而且能提前行程，这样比较好吧。也很在意勇者的事，而且还有作为从魔第一号的史莱姆。
一边想着事情，一边向城镇前进。
我们的冒险开始了。
