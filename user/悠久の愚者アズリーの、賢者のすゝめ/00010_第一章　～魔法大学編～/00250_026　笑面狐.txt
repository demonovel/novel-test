我们在迷宫裡果断实行了已经决定好的作战计划。
我给包括自己在内的小队成员全部都施与了能力提升和力量提升的叠加魔法。托star rod（星之杖）的福，使之能做到魔法的叠加，但为了将能力提升的魔法编入法杖中，火球的魔法就不得不被除外了。
在速攻魔法（高速发动魔法 swift magic）不能使用的现在，顷刻间的攻防中，让魔法阵或魔术阵形成是非常困难的。
[那麼……我上了！]
当波奇向笼罩着少许黑暗的迷宫外面跑出去的时候，立马便让敌人看到了它的巨大化。
已经读取了周围的情况了吗，朝着被认为是最弱的敌人所在的方向跳跃过去的波奇。从迷宫内看着那种情景的我们，在慢了数拍後也往外面出去了。
[咕啊啊！？]
布雷伊扎、布鲁兹、贝缇在形成将我围住的阵形之时，敌方中的一人已经被波奇的獠牙所咬住，然後波奇跳了起来将其撕碎。
[前方，五人！]
[左边，七人哦！]
[右边，除去一人之後五人！]
[後方、头顶上方都没有异常！]
也就是说有17人。
我们不得不在只有五人的情况下对付笑面狐的成员。
[波奇现在在右边！布鲁兹，小心谨慎的往左边移动一点！]
[喔！]
经验丰富的布雷伊扎所發出的指示的那个声音，仿佛包含有使士气上涨般的不可思议的力量。
从现在起才是胜负的关键！
我一边注意着波奇的损伤，一边投入到了魔法阵的制作中。与此同时，前方还有左边的敌人开始朝这边发动了攻击。
[哇啊啊！]
能听到被波奇的爪子所撕裂的男人的临死前所發出的痛苦声。
在前方最後边的男人發出了指示，让波奇所对峙的前方的四名战士对波比进行诱导作战。来迷宫的途中，没有对贝缇出手的那个男人，那傢伙果然是头领吗。
但是这样做的話，波奇的引诱便有了成效了。
[嗬，这边有八个人吗……]
[贝缇去游击，弄清楚波奇和布鲁兹的情况！布鲁兹，你想办法应付一下六个人！]
[[了解！]]
是因为是兄妹吗，还是因为长年交缘的原因，步调一致的两个人一起做了回应。布鲁兹对付六个人，也就是说在贝缇进行游击的同时，我和布雷伊扎两人就不得不在这裡坚守不动着。
布雷伊扎现在正在用等级感知对双方的战力平衡作出判断。该怎麼说呢，真是厲害的能力呐。
敌人的头领也行动了起来。布雷伊扎的对手包含头领在内共有三人。

[吓啊啊哦拉！]
剑与剑相互碰撞的声音，布鲁兹的战鬥开始了。被强化了的身体所施放出的剑击碰撞着敌人的剑。瞬间，敌人的剑便被弹飞，插入到迷宫入口旁边的岩石上了。
在防守的空隙下，敌人的脸上染现出了如同**般的痛苦表情，随後便被布鲁兹後方所飞过来的匕首给击中了。扎入脑袋的那把匕首，是直到刚才为止才被安装到贝缇腿上的东西。
[干得好，贝缇！]
[当一一然的啦！]
真是绝佳的配合。这是与我所穿过的战场数量不同所造成的差别。
[嗨唷！]
[呀！]
布雷伊扎也将走在前头的敌人一瞬间就给砍倒了。
波奇所在的方向也听到了某个人的悲鸣声。情况还不错。
[…………啧！把最優先对准那个魔法士！]
笑面狐的头领在看清战况後，把目标集中在了我的身上。看来对方也是身经百战的对方。
周围的敌人从这时起，把《将布雷伊扎他们打倒》作战改为了《限制布雷伊扎他们行动》作战，因为敌人觉得就算只有一点，只能接近我就足够了。
[啧！扣杀粉碎斩！]
从大剑裡所發出的布鲁兹的剑技，让离其最近的男人被其剑身所斥退，随後举高对其砍了下去。
因被无法认知威力程度的攻击而切断飞出去的敌人的上半身，在掉落在地面之後發出了微弱的呻吟声，随後便永远的沉默了。
战鬥仍然在继续着。虽然布雷伊扎巧妙的挡下了敌方头领的攻击，但却遭受到了另一人的同伴在不断战鬥的间隙中穿插进来的攻击。虽说没有大碍，但小打小闹的伤口却开始逐渐多得显眼起来。
[痛！]
从右侧听到了波奇的声音。是夹杂着痛苦的粗暴嘶哑的声音。
然而它却没有要求支援，因为它知道我们这边正處於没有那个餘力的情况中。无论是谁都尽量以最大限度的状态坚持战鬥着。
[咕！]
去援护放了大招的布鲁兹的贝缇，右大腿上出现了很深的伤口。布鲁兹马上赶到了贝缇的右侧。
[可恶！还没好吗，阿兹利！]
无法做出回应。此刻我的全部精力都集中在描绘魔法阵上。
像沉默的火、像停息的风、像漂动的水、像在大地扎根的巨木一般……！
[对敌陷阱！重力停止！]
瞬间，周围一带的空间被我所支配，我将敌方标记为《红色》、友方标记为《蓝色》进行区别後发动了效果。
[什麼！？]
如同要被吸入地面一样趴在地上的笑面狐成员一伙。
重力干涉系时空魔法《重力停止》。在以魔术的《六角结界》的基础上，配合出了只限定敌人为目标的重力地狱。

只要是稍微较强等级的人的話，就会无法支撑自身重量而趴倒在地上。
[咕……你、这**……做了什麼……啊？]
[你果然看起来还是能动的样子呢。]
[库、身体感觉好重……想动也动不了……]
[还是不要勉强比较好哦。即便你有体力支撑着也没用，因为大脑和内臟所带来的负载都是一样的。]
因为我的話，使得那个男人的脸歪曲了起来。严峻又顽强的那张脸上开始浮现出了急汗。
布雷伊扎一伙对於敌人所發生的异变，他们的理解能力终於似乎也追赶了上来。
[……这是，限制行动了吗？]
[差不多就是那种意思了。]
[阿兹利，效果持续时间是？]
[直到我的魔力用完的时候……暂时还是没有问题的。布雷伊扎桑，他们该怎麼办？]
布雷伊扎和布鲁兹，在我刚说完不久就开始将残留敌人的脑袋给砍掉。
露出了完全没有放鬆警惕、犹豫的表情。
[………呜哇。]
[这就是冒险者的世界哦，阿兹利君。犹豫不决的話可是会有导致一切全部都消失的情况。]
[您说得对。为了活下去只能这样了……]
[那麼，你就是笑面狐的头领、《玛乌斯》吗？以前在通缉令上有看到过你的事。]
布雷伊扎的剑搁在了玛乌斯的颈部。在其相反的一侧也已经增添了布鲁兹的剑，玛乌斯的脑袋现在正被两把剑所包夹着。
而贝缇则是带着很深的伤口去看波奇的样子如何。
[回答之前，先说先前往这裡来的小队怎样了？]
[……呣，即便不作回答，你也应该知道接下来我会做什麼的吧？毕竟这就是所谓的冒险者的世界。]
笑得很狰狞的玛乌斯，一边抬头一边蔑视着布雷伊扎。
[是吗。那麼至少在一瞬间给你个了断吧。]
布雷伊扎把剑高高抡起的瞬间，有小刀朝着他那脚边飞了过来。不对，我甚至连那是不是小刀都没能注意到。布雷伊扎在身体倒下的同时，布鲁兹也未能躲闪开攻击，被小刀将脚背与地面给"连接"在了一起。
当注意到的时候，玛乌斯的旁边已经站有一个女的了。
个头小而又年轻。以如同不能认为是活着一般无机质的表情，使布雷伊扎看了後也不敢轻举妄动。
把肩膀借给玛乌斯的那个少女，用不觉得是少女的力气有力而又轻快的跳跃着，从确认好了的退路中逃走了。
[……不小心给逃掉了呢。]
[啊，比起那个，脚好痛啊！快点帮我做点什麼吧！]
[阿兹利，先别管那边，波奇就拜托了！]
抱着恢復到原来大小的波奇的贝缇發出了着急的呼喊声。
对了，波奇！
一下子急忙赶到的我，看到了波奇在贝缇的膝盖上咻咻的困难的喘气着。
[哎呀……主人，我还活着啊……?]
[不要说話比较好。给我做鬼脸的练习。]
[啊哈哈，那个可……有点费力呢。]
虽然脉搏没有异常，但内臟却会产生疼痛。看来不得不从身体内部进行治疗回復。
[嘿咻嘿咻嘿咿（画魔法阵的声音），圣生结界。]
在波奇的身体下方刻上了魔法阵(翻译:原文是魔术阵，但考虑到後面的对話内容，在这裡给改成魔法阵，不知道这裡算不算是作者笔误。。。)，随之波奇和贝缇便被淡淡的光芒所包围了。
[……这是？感觉身体好像变得暖和起来了。]
[收缩了大地气息的这个……魔法阵…正在从中释放出，有着使身体内的细胞活性化的效果。因为根據波奇的这个状态，比起伤口来说，使体内的损伤康復的方法看起来更好。
[呼，看起来康復要花点时间呢。布鲁兹，给我照看一下她们！](翻译:这裡应该是布雷伊扎在说話)
虽然从远处回响起脚痛着的布鲁兹的悲壮声音，但谁也没有对此做出回应。

◇◇◇◇◇◇◇◇◇◇◇◇
[说真的，魔法什麼的好便利啊一一。我是不是也该坚持学一下比较好呢？]
隔天早上，贝缇冲着比起魔术还是魔法更快使其痊愈的脚说道。
[那麼，布鲁兹桑，请让我看一下您的脚。]
[现在才来看吗！]
[抱歉，因为之前顾着波奇的治疗……]
[唉，毕竟做着最不容易的任务。这也是没办法的嘛。]
抱着胳膊把受伤的脚给阿兹利看的布鲁兹，一边朝着後天要走的方向，一边那样说道。
不把使魔当作道具对待，能看见会这麼做的冒险者是非常少的。按理说，在大多应该让人类的治疗優先的这样判断的人群之中，布鲁兹的这个回应可以说是很少有的情况吧。
在魔法士里也是存在着将使魔当成道具的人的，真是让人头疼的事。
[真的很谢谢您的体谅。如果我的回復魔法能更加拿手的話就好了，但仍还在做修行中……]
[的确，因为主人的考虑不周，真是对不起了。]
[所以说为什麼是你在答話啊！]
[欸，重点是那个来着吗？]
[当然不是重点！]
[那不就没问题了吗？]
啊，真的呢。
(翻译:[欸，重点是那个来着吗？]
[当然不是重点！]
以上这两句是脑补翻的，因为原文真心弄不懂是什麼意思。。。。。。
原文:「え、至ってましたっけ？」
「至ってませんとも！」)
