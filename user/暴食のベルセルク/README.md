# novel

- title: 暴食のベルセルク　～俺だけレベルという概念を突破する～
- author: 一色一凛
- source: http://ncode.syosetu.com/n6887dt/
- cover: https://images-na.ssl-images-amazon.com/images/I/51qsyxoh6JL._SX350_BO1,204,203,200_.jpg
- publisher: syosetu
- date: 2018-05-27T23:00:00+08:00
- status: 連載

## series

- name: 暴食のベルセルク　～俺だけレベルという概念を突破する～

## preface


```
在城堡担任门卫的菲特有一个烦恼。
因与生俱来的【暴食】技能，而不断地受到空腹感的侵袭。虽认为是除了肚饿之外，就毫无用处的技能，但其实有著隐藏的力量。
某日，在杀死潜入城堡的盗贼时，菲特发现了其真正的力量。
吞噬被杀害者的灵魂，并把力量全夺取的技能……。然后，初次满足了空腹感。
以这件事为契机，被当成是无能和*****，逐渐崭露头角。后来超越了等级的概念，以永无止境地变强之姿植入敬畏之念，被称为……「暴食的狂暴」。

お城の門番をしているフェイトは、ある悩みを抱えていた。
生まれた時から持っている《暴食》スキルによって、絶えまなく襲ってくる空腹感だ。腹が減るだけの役に立たないスキルと思っていたが、実は隠された力があった。
ある日、フェイトは城に忍び込んだ盗賊を殺した時、真の力に気がついてしまう。
殺した対象の魂を喰らい、力をすべて奪ってしまうスキルだと……。そして、空腹感は初めて満たされていた。
このことをきっかけに、使えないやつだとゴミ屑のように扱われてきた男は、次第に頭角を現していく。のちにレベルの概念を越えて、際限なく強くなる姿から畏怖の念を込めて、こう呼ばれた……「暴食のベルセルク」と。
★★ GCノベルズより、第二巻が発売中です(2018年4月28日)。
★★コミックライドでコミカライズ開始。
```

## tags

- node-novel
- R15
- syosetu
- レベル完全無視
- 主人公最強
- 喋る武器
- 大罪スキル
- 残酷な描写あり
- 異世界

# contribute

- Nodius
- 北方
- 黄色柠檬_48
- fujjnhuy
- 真伪
- Mi-ku
- is007a
- 

# options

## syosetu

- txtdownload_id: 976790

## textlayout

- allow_lf2: true

# link

- [dip.jp](https://narou.dip.jp/search.php?text=n6887dt&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [暴食のベルセルク - pixivコミック](https://comic.pixiv.net/works/4258)
- [暴食のベルセルク～俺だけレベルという概念を突破する～ コミックライド](http://comicride.jp/gluttony/)
- [暴食的狂暴](https://tieba.baidu.com/f?kw=%E6%9A%B4%E9%A3%9F%E7%9A%84%E7%8B%82%E6%9A%B4&ie=utf-8 "")
- [狂怒的暴食 ～只有我突破了等级这概念～](http://www.dm5.com/manhua-kuangnudebaoshi--zhiyouwotupoledengjizhegainian/) - 漫畫
- 


