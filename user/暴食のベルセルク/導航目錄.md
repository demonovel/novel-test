# CONTENTS

暴食のベルセルク
暴食のベルセルク　～俺だけレベルという概念を突破する～


- [README.md](README.md) - 簡介與其他資料
- [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/user_out/%E6%9A%B4%E9%A3%9F%E3%81%AE%E3%83%99%E3%83%AB%E3%82%BB%E3%83%AB%E3%82%AF.epub) ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/user_out/out/%E6%9A%B4%E9%A3%9F%E3%81%AE%E3%83%99%E3%83%AB%E3%82%BB%E3%83%AB%E3%82%AF.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/tree/master)
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%9A%B4%E9%A3%9F%E3%81%AE%E3%83%99%E3%83%AB%E3%82%BB%E3%83%AB%E3%82%AF.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/tree/master/lib/locales)
- [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitee.com/bluelovers/novel/blob/master/user/暴食のベルセルク/導航目錄.md)  ![Discord](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://discord.gg/MnXkpmX)




## [null](00000_null)

- [第１話　持たざる者](00000_null/00010_%E7%AC%AC%EF%BC%91%E8%A9%B1%E3%80%80%E6%8C%81%E3%81%9F%E3%81%96%E3%82%8B%E8%80%85.txt)
- [第２話　蠢動的暴食技能](00000_null/00020_%E7%AC%AC%EF%BC%92%E8%A9%B1%E3%80%80%E8%A0%A2%E5%8B%95%E7%9A%84%E6%9A%B4%E9%A3%9F%E6%8A%80%E8%83%BD.txt)
- [第３話　スキル考察](00000_null/00030_%E7%AC%AC%EF%BC%93%E8%A9%B1%E3%80%80%E3%82%B9%E3%82%AD%E3%83%AB%E8%80%83%E5%AF%9F.txt)
- [第４話　強欲なる黒剣](00000_null/00040_%E7%AC%AC%EF%BC%94%E8%A9%B1%E3%80%80%E5%BC%B7%E6%AC%B2%E3%81%AA%E3%82%8B%E9%BB%92%E5%89%A3.txt)
- [第５話　喰い散らかす](00000_null/00050_%E7%AC%AC%EF%BC%95%E8%A9%B1%E3%80%80%E5%96%B0%E3%81%84%E6%95%A3%E3%82%89%E3%81%8B%E3%81%99.txt)
- [第６話　ハート家の裏側へ](00000_null/00060_%E7%AC%AC%EF%BC%96%E8%A9%B1%E3%80%80%E3%83%8F%E3%83%BC%E3%83%88%E5%AE%B6%E3%81%AE%E8%A3%8F%E5%81%B4%E3%81%B8.txt)
- [第７話　飢えに溺れる](00000_null/00070_%E7%AC%AC%EF%BC%97%E8%A9%B1%E3%80%80%E9%A3%A2%E3%81%88%E3%81%AB%E6%BA%BA%E3%82%8C%E3%82%8B.txt)
- [第８話　飢餓ブースト](00000_null/00080_%E7%AC%AC%EF%BC%98%E8%A9%B1%E3%80%80%E9%A3%A2%E9%A4%93%E3%83%96%E3%83%BC%E3%82%B9%E3%83%88.txt)
- [第９話　貪り喰う](00000_null/00090_%E7%AC%AC%EF%BC%99%E8%A9%B1%E3%80%80%E8%B2%AA%E3%82%8A%E5%96%B0%E3%81%86.txt)
- [第１０話　第一位階](00000_null/00100_%E7%AC%AC%EF%BC%91%EF%BC%90%E8%A9%B1%E3%80%80%E7%AC%AC%E4%B8%80%E4%BD%8D%E9%9A%8E.txt)
- [第１１話　一時の休息](00000_null/00110_%E7%AC%AC%EF%BC%91%EF%BC%91%E8%A9%B1%E3%80%80%E4%B8%80%E6%99%82%E3%81%AE%E4%BC%91%E6%81%AF.txt)

