【那麼、回到今天原本的本題吧】

的尼維這樣開口說道。
雖然有那麼一瞬、想著那是什麼來著、但是還是好好地記著的。
是關於塔拉斯孔的交易吧。
因為有說過要用對這邊有利的條件買下的⋯⋯會值多少錢啊？（譯：色をつけて這句意思是會給些方便的意思吧？語死早就腦補了）

最近因為不斷有想要的東西所以現在稍微有些缺錢。
金錢不管有多少都不足夠。
像是使用大地龍的《魔鐵》做出的裝備、還有比現在用著的還要大的魔法袋⋯⋯⋯
但是、

【原來確實有著想要買的意願嗎？稍微有些意外】

雖然也考慮了只是純粹當做把我給引出來的方便借口的可能性、看來好像不是這樣。

【誒誒、是想要買的哦。因為塔拉斯孔的素材還算是蠻貴重的嘛。尤其是毒腺。對吸血鬼很有效】

接著尼維便這樣回答了。

【塔拉斯孔的毒對吸血鬼⋯⋯？】

【這是沒有被普遍的情報哦、吸血鬼雖然是有著高耐性的魔物、但是塔拉斯孔的毒對它們確實是有效的。要是下級吸血鬼為對象的話身體會直接被麻痺。雖然對越是上位的吸血鬼效果會越來越難以發揮效用、但是還是比什麼都沒有要好的】

⋯⋯雖然對我是沒什麼效果啦。
何況剛才根據聖炎的判別也沒有暴露、我果然是和一般的吸血鬼有著很大的區別吧。
話說究竟是不是吸血鬼這一點說不定也應該要懷疑吧⋯⋯⋯
但是、要確實地判別出到底是什麼魔物的話、只有根據其外表和特徵與從古至今所收集的知識比照再進行判斷這一方法吧。
而其得出的則是「我是吸血鬼」這樣的結論。
在此之上已經沒有其他調查的方法了啊⋯⋯⋯
嘛、這樣的話也沒辦法吧。
何況現在去在意也是什麼辦法都沒有吧。
比起這個還是塔拉斯孔的價格比較重要啊。

【像是尼維大人這種等級的冒険者的話、塔拉斯孔這種程度是可以自己去狩獵的吧】

雖然確實是貴重、有著尼維這種程度的實力的話、話就不同講了吧。
因為這個原因、可能會使到金額下降所以感到若干不安從而提出的疑問。

【想要去狩獵的話是辦得到的吧。但是、因為塔拉斯孔是⋯⋯不管在哪個地區都是一樣的、都是棲息在讓人不想涉足的沼澤地區呢。況且事前準備也夠受的、比起這種事我還是比較想去狩獵吸血鬼。幸好、偶爾還是會有像這次一樣把塔拉斯孔狩獵回來的人物哦。不如說真虧雷特桑會有想要去那種地方的念頭呢？】

尼維這樣的尋問過來了。
塔拉斯孔的住處是由於塔拉斯孔自身所分泌的毒素、才會造成了那樣的有毒地帶。
因此、不管在什麼樣的地區尋找塔拉斯孔、必定會有通過那種環境的必要。
她厭惡的理由我也可以理解。
就算是尼維、要全身都塗滿毒和淤泥邊前進這樣的事也是想要避免的吧。
要說我的話就⋯⋯⋯
嘛、我也不是喜歡才自願去那種地方的。
只是⋯⋯⋯

【有一些這邊的理由、因為在那裡有著無論如何都想要採取的素材。該說是順便還是怎麼說好呢⋯⋯】

對於我的回答、尼維稍微思考了之後、

【⋯⋯呼姆、要說只能在塔拉斯孔的住處找到的素材的話⋯⋯像是《龍血花》、《毒浴鳥》、還有就是⋯⋯《塔拉斯孔的毒石》這幾種吧。哦、就是這幾種的其中之一吧。原來如此】

雖然自覺沒有表露出情感、但不知為何貌似尼維可以憑著剛才這些就理解了。

繆莉亞絲は、

「⋯⋯雰圍氣や仕草に全く変化などなかったと思いますが⋯⋯」

と俺の様子を評價したが、ニヴには分かってしまったらしいのでダメだ。
修行が足りないようである。

「さて、雑談はこんなところにしておいて、そろそろ商談と參りましょうか。シャール殿、今回の塔拉斯孔、オークションでは、いかほどで売れる予定でしたか？」

ニヴがシャールにそう尋ねると、彼は頷いて答える。

「通常、塔拉斯孔の素材は部位によって値段はまちまちですが、一匹丸々で言うと、平均から行って金貨６０～から２００枚の間で売卻されています。ただ、今回のものは狩り方や解體に全く無駄のない、綺麗な素材でしたので⋯⋯最低落札價格が金貨１００枚から始めることになったでしょう。そこからどこまで上がるかは時の運だったでしょうが、今回出席される方の顔ぶれからして、金貨３００枚よりも上がることは無かっただろう、と思います」

うーん、聞けば聞くほど俺のような一般庶民感覚からすると額がとてつもない。
塔拉斯孔はデカかったから、その分、一匹から採れる素材も多く、値段もつきやすいのかもしれないが、それにしても⋯⋯という感じだ。
やっぱり場所が場所だから、中々出回らないのだろうな⋯⋯⋯

「なるほど、でしたら⋯⋯予定では落札價格の倍額で購入するという話でしたから、金貨６００枚だった、というところから始めましょう。ただ、もしかしたら酔狂な人がいて、金貨４、５００枚になった可能性はないではありませんよね」

イヴがそう、シャールに言うと、シャールは難しそうな顔をして、

「まぁ⋯⋯そうですな。あまりないことですが、上げ幅の見通しを間違えて、１０枚上げるところを１００枚上げて青くなる方もいらっしゃるくらいですし。ただ、そういうことは起こっても一度でしょうな。ですから、金貨四百枚以上は絶對なかった、と言えるかと」

「では、倍額とは金貨８００枚ということで。それで、それに加えて私の誠意ですが⋯⋯通常、他人の生命を理由なく侵害した場合、賠償額は平民の場合はいくらでしたか？」

「⋯⋯金貨１０枚から５０枚の間が多いですな。職業にもよりますが⋯⋯私が殺された場合でも、金貨５０枚は超えないでしょう」

これは俺も知っている話だ。
平民の命の價格は低い。
まぁ、それでもそれだけあればたとえば遺族が主を失って生活が安定するまでの時期を乗り越えてやっていくくらいには十分事足りる額でもある。
しかし、都市馬路特において、これだけ大きな商會の主であってもその額ということは、俺の場合は⋯⋯⋯
まぁ、もっとずっと低いのは間違いないだろうな。
なにせ、明日をも知れぬ冒険者だ。
とはいえ、そういった場合の賠償額は領主が決めるのが普通で、建前上、同じ身分間では命の價値に差はないとされているから、額にあまり大きな差はない。
シャールが死んだ場合と俺が死んだ場合とでは、社會的損失に大きな違いがあるだろうが、そういうことはあまり考慮されないのだ。
これが貴族が死んだ場合となるとまた大きく話は変わってくるのだが、まぁ、それは今はいいだろう。
ニヴは言う。

「では、雷特さんが殺された場合の遺族に對する賠償額は金貨５０枚だったということにしましょう。私はそのようなことをしようとしていたわけで、しかも結果的に問題なかったとはいえ、だまし討ち紛いのことをしているわけですから⋯⋯これも、その倍額ということにいたしましょうか」

つまりは金貨１００枚を賠償額だと仮定すると。
高いなぁ、と思う反面、塔拉斯孔をこれから何體も狩れたのに殺されてその可能性をゼロにされてたわけだから、まぁ、妥當と言えば妥當か。
領主によって判斷される、通常の場合には考慮されないところなので、良心的であろう。

「⋯⋯合計金貨９００枚、ということですか」

ううむ、それだけもらえれば俺の欲しいものは全部手に入りそうだ。
まぁ、魔法の袋で大半消えて、殘りで武具を買うくらいになるだろうが⋯⋯⋯
魔法の袋は高すぎるんだよな。
それでも今持っている奴の三倍くらいの容量の奴しか買えないだろうから。
塔拉斯孔くらい入る奴は⋯⋯うーん、前オークションで見たときは、金貨１８００枚くらいの値段がついてたなぁ⋯⋯無理だな。
ニヴの提示に不滿という訳ではないが、これからそのために金貨の貯金をするか、それとも諦めて小さめの魔法の袋を買うかで迷って、妙な雰圍氣を出してしまったらしい。
ニヴはそれを勘違いしたのか、

「⋯⋯足りませんか？　まぁ、そうですよね⋯⋯では、こうしましょう。キリよく金貨千枚、ということでいかがです？」

と、いきなり百枚上げてきた。
どんだけ金を持っているんだ、こいつは⋯⋯⋯
いや、吸血鬼狩りの報酬はかなりいい。
それを頻繁にこなしているだろうことを考えるとおかしくはないか。
しかし⋯⋯そんなにもらっていいのだろうか？
俺はちょっと悩む。
別にお金が嫌いな訳じゃない。
そうではなく、ニヴからあまり大量のお金をもらうと⋯⋯なんだか繋がりの糸みたいなのが強くなっているようで出來れば避けたいなと思ってしまうのだ。
まぁ⋯⋯連絡先もらっているのだから、今更なのかな？
いやいや、でもなぁ⋯⋯⋯

と悩んでいると、ニヴはさらに勘違いしたのか、仕方なさそうに笑い、

「全く、交渉上手ですね、雷特さんは⋯⋯分かりました。さらに倍、出しましょう。金貨二千枚、これ以上はもう出せませんよ。流石に私にも今後の生活がありますからね」

と言って、機の上に白金貨を二十枚並べる。
⋯⋯白金貨。
こんなに。
はじめてみた⋯⋯⋯

心の中では唖然としていた俺である。
しかし、雰圍氣には出さない。
今度ばかりは鉄の意志で表にはどんな雰圍氣も出さなかったため、ニヴにも分からなかったようで、

「銅級ですのに、これを見てもそこまで不動の人は中々いませんよ。雷特さんは吸血鬼でこそなかったようですが、やっぱり、かなり稀有な人のようですね」

と妙な方向に感心していた。
ニヴの隣の繆莉亞絲は、まさに唖然とした顔で口をぽっかり開けている。
シャールの方は、何か頭を振って、呆れたような顔をしていた。
ニヴの財力にか、ここまで額面を上げさせたように見える俺の態度にか⋯⋯⋯