# 含有日文的章節段落

[TOC]

## 00070_第７章　銅級冒険者レント/00670_第６７話　銅級冒險者雷特與鼠

- '從魔師的特殊魔術基本等同於秘藏，そうそう的學會只不過是艾莉婕的誤會，特此訂正。'


## 00070_第７章　銅級冒険者レント/00700_第７０話　銅級冒險者雷特與名字

- '「其他還有根據體型，意味胖子意思的莫普【モッペル（德語：圓胖的小孩）】，意味食量很大的富利薩（フレッサー）等也不錯」'


## 00080_第８章　奇妙な依頼/00190_第１０３話　奇妙な依頼と種族

- '羅蕾露はなんとなく検討がついているような感じだったので、分かるか、と目配せで聞いてみるも，但她傾斜她的脖子和眨眼'
- '「應該沒有辦法。從來沒有聽說過有羽毛的吸血鬼之類. 你對一個沒出色的學者抱太大期望了。那麼⋯⋯ 你無疑作為魔物是個異類。因為一般的種族分類之類從一開始就不適用，怎麼想也沒用. 這樣的結論應該是對的. あくまであえて當てはめるのなら、その邊だろうな、という話だ。」'
- '這是一個小小的諷刺，分かっているのなら言うな、と言いたげである。'
- '「⋯⋯それは勘弁願いたいな」'
- '出來ることなら、このまま見た目は変わらないでほしいものだな⋯⋯⋯'


## 00090_第９章　下級吸血鬼/00010_第１０４話　下級吸血鬼と師弟の誓い

- '羅蕾露ぎぎぎぎ，回著頭望著我時驚慌似的道著。'
- '「はい、哪位客人⋯⋯啊、雷特！」'


## 00090_第９章　下級吸血鬼/00060_第１０９話　下級吸血鬼と許可

- '「えー！約束したのは私が先だもん！」'
- '五歳くらいの男の子で、俺が手持無沙汰に飛空艇をここで飛ばし始めたのを羨ましそうに見ていたので、少し貸しているのである。'
- '「⋯⋯しかし、いいのか？　あれはお前にとって大事なものではなかったか」'
- '羅蕾露がびゅんびゅん飛ぶ飛空艇を見ながら、そう呟く。'
- '「いいんだよ。みんな楽しそうだろ？」'


## 00090_第９章　下級吸血鬼/00160_第１１９話　下級吸血鬼と節約

- '手をたどって馬路特や、亞蘭から外へ逃がすことも出來る。實在不會了'


## 00090_第９章　下級吸血鬼/00170_第１２０話　下級吸血鬼と慣習

- '多數是中級吸血鬼為盟主而君臨的群中有數體下級吸血鬼（レッサー・吸血鬼），並且在那下面有被稱為屍鬼而使役的人，這種情況是一般的。即使捕獲了一只下級吸血鬼，也會因作為聯盟主的中級吸血鬼而暴走，被爆散波及。'


## 00090_第９章　下級吸血鬼/00180_第１２１話　下級吸血鬼と眷属の進化

- '「ぎぎっ！」'
- '真是一幅很奇異的景象（グロ：名・形容動詞/ナ形容詞 參見：グロテスク【英】Grotesque ;西洋穴怪圖像。為歐洲十五世紀末至十九世紀前半葉室內藝術特有的裝飾風格。在歐語系的文學用辭上，該字亦有奇異與怪誕之意）。'
- '「⋯⋯洗浄（リンピオ）」'


## 00090_第９章　下級吸血鬼/00190_第１２２話　下級吸血鬼と兵士

- '話雖如此，但並不是像我那樣從背上伸長出來，而是像ムササビ一樣，從腋下到後腳為止，從皮膚延伸出來的薄膜那樣的感覺。'
- '仮にも俺の眷屬であるわけだから、目は夜目の方が効くかもしれない。'
- '「⋯⋯ぶしゅるるるるる！！」'
- '「ぐぶぶっ！ぶるるるる！！」'
- '「ぎぎぶっ！ぶぶぶぶっ！！」'


## 00090_第９章　下級吸血鬼/00200_第１２３話　下級吸血鬼と豚鬼兵士

- '「ヂュヂュッ！」（鼠聲）。'
- '「ぷぎぃ！」（豬叫）'
- '──ずがっ！'


## 00090_第９章　下級吸血鬼/00250_第１２８話　下級吸血鬼と階段周辺

- '①分別是（エボニー）和（モミノキ），不知道是什麼樹就音譯了'
- '②做不動了，那句純屬蒙的⋯（いくら早いと言っても、まっすぐ突っ込んでくるだけなら待ち伏せて叩き伏せるだけで済む）'


## 00090_第９章　下級吸血鬼/00290_第１３２話　下級吸血鬼と幻影

- '不過，為了探明原因總之再試著進行了一次挖掘。（けれど、とりあえず一度採掘してみて、どんなものかは分かっておきたかったためにやってみただけだ。）'


## 00090_第９章　下級吸血鬼/00350_第１３８話　下級吸血鬼と取り込まれるもの

- '「有哦！除此之外，還有其他的例子。你知道曾經在迷宮建立一所王国的費王費魯特【フェルト】的事嗎？」'


## 00090_第９章　下級吸血鬼/00360_第１３９話　下級吸血鬼とロレーヌの昔語り

- '「是雷魯門多【レルムッド】帝国吧？」'


## 00090_第９章　下級吸血鬼/00370_第１４０話　下級吸血鬼と素材

- '今日的預定是拍賣會⋯⋯就是說、到蘇特諾商會（ステノ商會）去商談關於塔拉斯孔的素材吧。'


## 00090_第９章　下級吸血鬼/00380_第１４１話　下級吸血鬼と木材

- '【這個確實是、也不能這麼說吧（譯：原文是：確實にこれ、というわけではないのだが。不是很明白為什麼說這句）⋯⋯比如說、做成劍的話、只要來回揮動不需要使用魔力也可以放射出岩槍之類的、這一類的東西吧。也就是說、做出魔劍或是魔盔之類的可能性很高】'
- '但是、想要這之後就去商量。（だが、相談だけは後でしておこうと思った。）'
- '【樺木（シラカバ）、烏木（エボニー）、還有⋯⋯冷杉（モミノキ）嗎。雖然還稍微想著說不定是拿著以前拿過來的材料、但意外的拿來了不錯的材料嘛？】（譯：名字純機翻、完全不想找、在意的請自行Google吧）'
- '【可以這樣說吧。（まぁな）但是、如果是你來使用的話就不同了。腕力也有、相當粗暴的使用方式也可行吧。即便如此因為是強韌的木材所以不會壊吧。是適合你使用的杖。但是、加工可不容易ぞ（Zo）。極具挑戰性啊、雷特（挑戰だな、雷特。譯：不是很確定這句對不對）】'
- '（譯：。這一整段讀到很傻⋯還以為なぁなぁ是作者打錯字查了才知道是方言、這一整段都不是很明白）'


## 00090_第９章　下級吸血鬼/00390_第１４２話　下級吸血鬼と精錬

- '羅蕾露在這方面來說、是極其優秀的魔法師、感覺只要在閑餘時間就可以輕鬆（ぱぱっと）地解決了。'
- '接著便Shurushuru（しゅるしゅる）地像蛇一樣的動作著、從礦石隔離開了。（譯：這種聲音就直接音翻了、能力不足不懂怎樣翻成中文）'
- 'Ki、kikin（キン、キキン）、的發出聲音、微小的破片被彈出是因為不純物被隔離開了。'
- '【那麼、Saku Saku（サクサク）地做吧。】（譯：查了意思可是還是不明白就直接音譯了。）'
- '【⋯⋯好了、也就這樣吧（よし、こんなところだな）】'


## 00090_第９章　下級吸血鬼/00400_第１４３話　下級吸血鬼と商会

- '【知道了啦⋯⋯就這樣（じゃあな）】'
- '長袍的製作雖說品質很好但卻樸素而面具則是太過花哨導致引人注目、這樣模棱兩可的我要說的話果然還是很顯眼。（譯：原文是ローブは仕立てがいいけれど地味なのに仮面は派手に主張しているという、どっちつかずな俺はやっぱり目立つ。不確定對不對）'
- '【已經被告知過了。請往這邊走】（譯：原文是很有禮貌的發言、但同樣翻不出囧）（伺っております。こちらへどうぞ）'
- 'がちゃり（GACHARI譯：門打開的聲音）、門打開了、因為被催促著進去裡面、我便進去了。'
- '不客氣地ばくばく（Baku Baku 譯：應該是形容吃東西的聲音）吃著、こんこん（Kon Kon譯：敲門聲）、由於門被敲了、我被嚇一跳後慌忙地把茶具之類的放下、一邊盡可能謹慎邊以讓冷靜的語氣說道。（譯：這句只翻出大意、作者的想像力都用在怎麼把句子弄成看起來高級了）'


## 00090_第９章　下級吸血鬼/00410_第１４４話　下級吸血鬼と商会の主

- '無論如何、完全不覺得可以大意啊、的想著會讓人泄氣想法的我。（譯：這句只翻得出大意⋯語死了。下面放原文）（原文：どちらにしろ、全く氣が拔けなさそうだな、とげんなりした思いを覚えた俺だった。）'
- '還是雷特・梵納的時候倒是有著相當的傳聞、但現在。（今はなぁ）（譯：今はなぁ該怎麼翻啊、好難啊啊啊）'
- '嘛、在冒険者公會裡只和席伊菈說過話、大致上都是ぼそぼそ（BOSO BOSO）（譯：這裡是指小聲說著吧）小聲說著。'
- '【畢竟也不是什麼特別需要隱瞞的事⋯⋯雷魯門多的機械都市阿瓦安（アーヴァン）出身唷。您知道嗎？】'
- '與這些說明、連同從羅蕾露描述的阿瓦安（アーヴァン）的氣氛和氛圍一起傳達給他後、沙魯貌似信服了。'


## 00090_第９章　下級吸血鬼/00420_第１４５話　下級吸血鬼と二人の人物

- '──コンコン。（Kon Kon 譯：敲門聲）'
- '既保有著一定的強度、還持有著可以把隱藏踪跡住著的吸血鬼給找出來的卓越技能也是通過這件事才廣為人知的。（譯：その強さもさることながら、隠れ住んでいる吸血鬼探しに卓越した技能を持っている、ということがそれでわかったからだ⋯不明白就腦補了）'
- '【報上姓名的時機有些遲了。我（わたくし譯：典型的貴族語氣不是Watashi而是Watakushi）是就職于洛貝利亞教的神官、名叫穆利烏斯・麗莎。請多指教。】（譯：高級感滿溢的句子翻不出味道只留下意思了⋯）'


## 00090_第９章　下級吸血鬼/00430_第１４６話　下級吸血鬼と判別の試し

- '注：另外，關於吸血鬼獵人「ニヴ」的翻譯問題，按羅馬音念的話，應該是「Nibu」，但實際上『ヴ』這個片假名，在日語中被使用時，有一些特殊規則'


## 00090_第９章　下級吸血鬼/00470_第１５０話　下級吸血鬼と金勘定

- '塔拉斯孔はデカかったから、その分、一匹から採れる素材も多く、値段もつきやすいのかもしれないが、それにしても⋯⋯という感じだ。'
- 'イヴがそう、シャールに言うと、シャールは難しそうな顔をして、'
- 'まぁ、それでもそれだけあればたとえば遺族が主を失って生活が安定するまでの時期を乗り越えてやっていくくらいには十分事足りる額でもある。'
- 'ううむ、それだけもらえれば俺の欲しいものは全部手に入りそうだ。'
- 'そうではなく、ニヴからあまり大量のお金をもらうと⋯⋯なんだか繋がりの糸みたいなのが強くなっているようで出來れば避けたいなと思ってしまうのだ。'


## 00090_第９章　下級吸血鬼/00520_第１５５話　聖女ミュリアス・ライザ２(前)

- '「如果想確實地傳播我們的教義的話，不應該是我而應該派遣亞魯茲（アールズ）大人和米莉雅（ミリア）大人來才對吧」'
- '「大教父廳發來的直接指令呢⋯⋯⋯這是⋯⋯去當金級冒険者尼維・瑪麗斯（ニヴ・マリス）的隨從？究竟是⋯⋯」'


## 00090_第９章　下級吸血鬼/00530_第１５６話　聖女ミュリアス・ライザ２(中)

- '「那是當然。就算是我，也不是一開始就是這樣（こう（⋯））的哦」'
- '整個人的氣氛⋯⋯感覺有點像艾魯茲（アールズ）大人。'
- '「啊—、當然、雖然也是我的目的之一，不過最重要的還是吸血鬼的狩獵。我呢，正在懷疑那個冒険者是否是吸血鬼。色々調べてほとんど確信に近いのですよ經過多方調查大致上確定了。所以，做好萬全準備後，想要將其了結呢」'
- '「不用那麼慌張啦。經常有的事情。言いましたが剛才也說過吧，他們是很狡猾的。混入城市的人群中，對他們來說輕而易舉。這麼說來，希望繆莉婭絲大人也能協助我進行吸血鬼的退治，畢竟羅貝莉亞教裡也有專門消滅邪惡魔物的獵人，可以說也是工作的一部分吧」'


## 00090_第９章　下級吸血鬼/00540_第１５７話　聖女ミュリアス・ライザ２(後)

- '尼維在即將與那個冒険者碰面的時候，在作為交涉場地的斯特諾（ステノ）商會前這般叮囑到。'
- '實際上，就曾有過被稱為《傾城的阿德涅（アドネー）》的這樣一個人物。'
- '由蘇特諾商會的店員引路，所來到的房間商會主沙魯・蘇特諾（シャール・ステノ）和另一位冒険者已在其中。'


## 00090_第９章　下級吸血鬼/00560_第１５９話　下級吸血鬼と諸々の問題

- '「そうはいっても、ラムズ大森林とか、ホヘル空中遺跡とかのような人外魔鏡というわけでもあるまい？」'
- '言われてみると、それはそうかもしれない。'
- '「惡くないかもしれないが、俺には依頼があるからな。そっちの方で許可を得ないと無理だぞ」'
- '「他にもこまごまとしたことはあるが、その邊りはどうとでもなるだろう。あとは、そうだな⋯⋯あぁ、お前、どういう立場で行く？」'
- 'もしものときはもしものときで、交渉の餘地もあるしな⋯⋯⋯'


## 00090_第９章　下級吸血鬼/00570_第１６０話　下級吸血鬼と資料

- '「雷特さん、ようこそいらっしゃいました⋯⋯今日は《龍血花》の納品ではなく、何かご用件があるとのことで⋯⋯？」'
- 'それなのに、それをしないで俺が馬路特に帰って來た後、まだ任せてくれるつもりであることがありがたかった。'
- 'なるほど、そうだろうな、という話だ。'
- 'あそこまでいかないにしろ、武器や身體能力の強化、それに治癒・浄化以外にも聖氣には使い方があるはずだ。'
- '秘匿されているのではなかったのだろうか。'


## 00090_第９章　下級吸血鬼/00580_第１６１話　下級吸血鬼と民話

- '「最後些書，是記載了今後雷特先生要前往的哈特哈萊村（ハトハラ—の村）及其周邊的民間故事之類。數量不多，但我想也許會派上用場」'


## 00090_第９章　下級吸血鬼/00620_第１６５話　下級吸血鬼と鍛冶屋

- '最著名的例子就是「ネプチューンの持つ三叉の銛」'
- 'ネプチューン，羅馬神話中的海王神尼普頓'


## 00090_第９章　下級吸血鬼/00640_第１６７話　下級吸血鬼と樹木

- '「那是魔鐵麼，產地是新月的迷宮嗎？還是哈姆丹礦山？」（ハムダン鉱山）'
- '①黃銅，原文為「真鍮」（しんちゅう）'
- '似乎與黃銅（おうどう）有什麼微妙的修辭界限上的不同，此處不表。'
- '②古貴聖樹国，原文為「古貴（こき）聖樹国」'


## 00090_第９章　下級吸血鬼/00660_第１６９話　下級吸血鬼と武器選択

- '②短劍，原文為「ダガー」，Dagger'


## 00090_第９章　下級吸血鬼/00670_第１７０話　下級吸血鬼と武器決定

- '②以防萬一，原文「潰しが効く」，多才多藝。（並非誤譯，而是有意為之）'


## 00090_第９章　下級吸血鬼/00690_第１７２話　下級吸血鬼と杖

- '②不明覺厲，原文「ちんぷんかんぷん」'
- '辭典：話している言葉や內容が全くわからないこと。話が通じないこと。'


## 00090_第９章　下級吸血鬼/00700_第１７３話　下級吸血鬼と失敗

- '「雖然有很多種⋯⋯魔法師之間最有名的一個傳說是，以前有個沒有繪畫才能的魔術師孔拉（コンラー）。因為嘴很巧，成為了宮廷魔術師。有一天，那個国家要進行傳承的儀式，讓他來擔任主持。但這個儀式上，需要繪制魔法陣，放出煙火。孔拉知道，自己沒有繪畫的天賦，不過就算失敗了，到時候適當的用魔法打出煙火就沒問題了。他就這樣擔任了主持。然後，實際上畫出的魔法陣確實發動了，但根據傳言，是發生了非常不妙的事情。你覺得，會是什麼樣的事情呢？」'
- '①走鋼絲一樣全神貫注，原文「プルプルしながら頑張っている」'
- 'プルプル，顫抖、哆嗦的意思'
- '②丟光，原文「丸つぶれ」，同「丸潰れ」'
- '（１）完全崩潰，完全倒塌，完全垮台。（完全につぶれること。）'


## 00090_第９章　下級吸血鬼/00720_第１７５話　下級吸血鬼と人形

- '①不太好，原文「格好つかない」'
- '「格好」這兩個日本漢字寫作假名的話就是「かっこう」'


## 00090_第９章　下級吸血鬼/00740_第１７７話　下級吸血鬼と完成

- '辭典：小さな布袋に小豆などを入れた玩具。これを數個、歌に合わせて投げ上げたり、受けたりして遊ぶ。また、その遊戯'
- '②這句話原文是「竜ではなく、《龍》だ」'
- '③無語的表情，原文「あきれ顔」'
- '「あきれ」有不同的日本漢字寫法「呆れ」、『厭きれ』、『飽きれ』'


## 00090_第９章　下級吸血鬼/00780_第１８１話　下級吸血鬼と乘勝追擊

- '①乘勝追擊，原文「ダメ押し」'
- '「ダメ押し」，同「だめおし」、『駄目押し』'
- '１、念のためもう一度確かめておくこと。'
- '２、スポーツの試合で、勝負がほとんどきまっているのにさらに得點を加え、勝利を決定的にすること。'
- '②世上無難事，只怕有心人。原文「やってやれないことはないだろう」'


## 00090_第９章　下級吸血鬼/00790_第１８２話　下級吸血鬼と身分

- '斯特諾（ステノ）商會之後，手拿魔法袋時有一瞬間浮現出這樣的感想。'
- '所說的事情是妮姆（ニヴ）已經證實了・所以現在的話，有這樣的想法。'
- 'になったそのときにはすでに落ち著いていて、他の都市の冒険者組'
- '合公會よりも遙かにマシな冒険者公會公會がここに出來ていた。'
