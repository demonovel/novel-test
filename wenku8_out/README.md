# toc

## OVERLORD不死者之王

- link: [OVERLORD不死者之王](OVERLORD%E4%B8%8D%E6%AD%BB%E8%80%85%E4%B9%8B%E7%8E%8B/)
- link_source: [OVERLORD不死者之王](../wenku8/OVERLORD%E4%B8%8D%E6%AD%BB%E8%80%85%E4%B9%8B%E7%8E%8B/)
- tags: 丸山くがね , node-novel , wenku8

### titles

- OVERLORD不死者之王
- オーバーロード

## 勇者、或いは化物と呼ばれた少女

- link: [勇者、或いは化物と呼ばれた少女](%E5%8B%87%E8%80%85%E3%80%81%E6%88%96%E3%81%84%E3%81%AF%E5%8C%96%E7%89%A9%E3%81%A8%E5%91%BC%E3%81%B0%E3%82%8C%E3%81%9F%E5%B0%91%E5%A5%B3/)
- link_source: [勇者、或いは化物と呼ばれた少女](../wenku8/%E5%8B%87%E8%80%85%E3%80%81%E6%88%96%E3%81%84%E3%81%AF%E5%8C%96%E7%89%A9%E3%81%A8%E5%91%BC%E3%81%B0%E3%82%8C%E3%81%9F%E5%B0%91%E5%A5%B3/)
- tags: 七沢またり , R15 , node-novel , syosetu , wenku8 , 勇者 , 化物 , 地下迷宮 , 女主人公 , 残酷な描写あり , 異世界 , 魔物

### titles

- 勇者、或いは化物と呼ばれた少女
- 被稱為勇者亦或是怪物的少女
- 勇者或是被稱為怪物的少女
- 勇者，或被稱為怪物的少女

## 瀆神之主

- link: [瀆神之主](%E7%80%86%E7%A5%9E%E4%B9%8B%E4%B8%BB/)
- link_source: [瀆神之主](../wenku8/%E7%80%86%E7%A5%9E%E4%B9%8B%E4%B8%BB/)
- tags: 榊一郎 , node-novel , wenku8

### titles

- 瀆神之主
- イコノクラスト!
