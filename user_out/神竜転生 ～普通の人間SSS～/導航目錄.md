# CONTENTS

神竜転生 ～普通の人間SSS～
神竜転生 ～普通の人間になるため転生したら全パラメーターSSSでした～


- [README.md](README.md) - 簡介與其他資料
- [含有原文的章節](ja.md) - 可能為未翻譯或者吞樓，等待圖轉文之類
- [待修正屏蔽字](%E5%BE%85%E4%BF%AE%E6%AD%A3%E5%B1%8F%E8%94%BD%E5%AD%97.md) - 需要有人協助將 `**` 內的字補上
- [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/user_out/%E7%A5%9E%E7%AB%9C%E8%BB%A2%E7%94%9F%20%EF%BD%9E%E6%99%AE%E9%80%9A%E3%81%AE%E4%BA%BA%E9%96%93SSS%EF%BD%9E.epub) ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/user_out/out/%E7%A5%9E%E7%AB%9C%E8%BB%A2%E7%94%9F%20%EF%BD%9E%E6%99%AE%E9%80%9A%E3%81%AE%E4%BA%BA%E9%96%93SSS%EF%BD%9E.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/tree/master)
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E7%A5%9E%E7%AB%9C%E8%BB%A2%E7%94%9F%20%EF%BD%9E%E6%99%AE%E9%80%9A%E3%81%AE%E4%BA%BA%E9%96%93SSS%EF%BD%9E.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/tree/master/lib/locales)
- [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitee.com/bluelovers/novel/blob/master/user_out/神竜転生 ～普通の人間SSS～/導航目錄.md)  ![Discord](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://discord.gg/MnXkpmX)




## [null](00000_null)

- [01 因為戰鬥結束，所以想變成人類](00000_null/01%20%E5%9B%A0%E7%82%BA%E6%88%B0%E9%AC%A5%E7%B5%90%E6%9D%9F%EF%BC%8C%E6%89%80%E4%BB%A5%E6%83%B3%E8%AE%8A%E6%88%90%E4%BA%BA%E9%A1%9E.txt)
- [02 有三個S並排着呢](00000_null/02%20%E6%9C%89%E4%B8%89%E5%80%8BS%E4%B8%A6%E6%8E%92%E7%9D%80%E5%91%A2.txt)
- [03 這種程度很普通啊](00000_null/03%20%E9%80%99%E7%A8%AE%E7%A8%8B%E5%BA%A6%E5%BE%88%E6%99%AE%E9%80%9A%E5%95%8A.txt)
- [04 傳說的道具裝備後力量下降了，不會不小心就毀滅世界了](00000_null/04%20%E5%82%B3%E8%AA%AA%E7%9A%84%E9%81%93%E5%85%B7%E8%A3%9D%E5%82%99%E5%BE%8C%E5%8A%9B%E9%87%8F%E4%B8%8B%E9%99%8D%E4%BA%86%EF%BC%8C%E4%B8%8D%E6%9C%83%E4%B8%8D%E5%B0%8F%E5%BF%83%E5%B0%B1%E6%AF%80%E6%BB%85%E4%B8%96%E7%95%8C%E4%BA%86.txt)
- [05 到達冒險者公會](00000_null/05%20%E5%88%B0%E9%81%94%E5%86%92%E9%9A%AA%E8%80%85%E5%85%AC%E6%9C%83.txt)
- [06 與公會的支部長進行腕相撲對決](00000_null/06%20%E8%88%87%E5%85%AC%E6%9C%83%E7%9A%84%E6%94%AF%E9%83%A8%E9%95%B7%E9%80%B2%E8%A1%8C%E8%85%95%E7%9B%B8%E6%92%B2%E5%B0%8D%E6%B1%BA.txt)
- [07 對劍進行附魔](00000_null/07%20%E5%B0%8D%E5%8A%8D%E9%80%B2%E8%A1%8C%E9%99%84%E9%AD%94.txt)
- [08 虽然是转生的第一天，也遇到了各式各样的事](00000_null/08%20%E8%99%BD%E7%84%B6%E6%98%AF%E8%BD%AC%E7%94%9F%E7%9A%84%E7%AC%AC%E4%B8%80%E5%A4%A9%EF%BC%8C%E4%B9%9F%E9%81%87%E5%88%B0%E4%BA%86%E5%90%84%E5%BC%8F%E5%90%84%E6%A0%B7%E7%9A%84%E4%BA%8B.txt)

