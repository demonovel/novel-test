（这裡我要解释一下腕相扑，简而言之就是掰手腕！手腕！腕！腕……）

「喂，那个，这是怎么了啊」

其他接待小姐看着散落满地的水晶碎片，都好奇发生了什么事情。

「……那个，这个孩子碰到水晶的时候，不知怎的就碎掉了……」
「欸！？难不成是拥有Ｂ等級魔力的人！？」
「不会吧，即使是Ｂ等級，也不会变成这个样子的，应该是坏了吧。稍微帮忙收拾一下吧。啊，艾莉桑，没有受伤吧？没事么？」

艾莉與神龍的时候相比大幅度的变弱了。
再加上克雷阿蜜拉给的吊坠，有大幅度的弱体化了。
即便如此，在水晶破碎的情况也是不会受伤的。
即使受伤了，也会瞬間再生的，谁都不会注意到。

「是的，我没事」
「是么，那太好了，你这样可愛的女孩子如果受伤了的話就不好了呢……虽然不是対志愿当冒険者的孩子该說的話呢」

接待小姐苦笑道。
接着将接待任务交给一个人，剩下的两人用扫帚和簸箕将破碎的水晶碎片收拾干净。

「嗯？突然开始打扫了，是发生了什么事情了么？」

在那裡的，是一个壮硕的男子。
年龄是五十岁前後。
長着壮观的胡子以及不输给胡子的壮观的肌肉。
是个像熊一样的男人。（译者：优奈表示这种熊熊并不需要）

但是，看其穿着，给人一种粗暴的印象。

「啊，支部長。观测水晶好像出了故障。这孩子一碰就碎掉了」
「故障？咦？才使用了几年而已，难道混入了劣质品么？这次看来得向总部提出索赔呢。我来拿新的水晶吧」
「得救了，支部長」

看来，这个像熊一样的男人是公会裡很伟大的人呢。
但却自己出去买杂物。
弱气，被部下任意使唤──的样子好像没有。
单纯是直爽的性格的样子。

「艾莉桑，刚才的是这个城镇冒険者公会最伟大的人，支部長哟。虽然我觉得新人并没有什么机会和支部長說話，但是机会难得，得记住他哦。还有，刚才和你說这个城市中有一个Ｃ等級魔力的人吧，說的就是支部長哟。最強大，最伟大，不论是公会的职员还是冒険者们，都很尊敬他哟」
「原来如此，我明白了」

艾莉没有深入思考地点了点头。
接待小姐们打算完后，正好支部長带着新的水晶回来了。
在水晶上，「C」这样的文字高高的浮现出来。
如果能好好测量魔力的話会这样现实的么？

「谢谢，支部長」
「没什么，能稍微减少忙碌的你们的负担的話，圆滑是公会运营最重要的东西了呢。接下来，让我们看看让水晶破碎的少女的魔力等級是多少吧」
「真是的，支部長，不是这个孩子弄碎的，是因为故障所以才会碎掉的啦」
「我知道的哦，但是……怎么說呢。我从这个少女的氛圍中感受到了不可思议的气息。将来一定是不得了的大人物」（译者：为啥不认为现在就是不得了的大人物呢？）

「嘛，支部長这么說的話，也许就是那样吧。但是……艾莉桑。即使真的拥有作为冒険者的才能，但是现在还很小，勉強的事情不能做哦！」
「Umu，正如阿忒露（アデル）桑所說的，无论多么強的冒険者，也会因为一时的疏忽而丧命的。疏忽是大忌。嘛，变成說教了呢。我已经确认过了这个水晶能正常运作，接下来就轮到你了」（译者：这裡叫阿忒露用的是君，这裡是対晚辈的称呼，不过因为是妹子，我就用桑了）

「那么」

艾莉再一次触碰了水晶。
啪哩噫噫噫吟！（バリィィィィン！）（译者：水晶破碎声）

又变得粉碎了呢。

「……那个。这个水晶也坏了么？」

艾莉认真地询问。
但是，阿忒露和支部長看着水晶的碎片，没有回答。
于是，艾莉将視线转向其他的接待员。
但是，她们也是一脸呆滞。
不仅如此，在接待処附近的冒険者们也一声不响的固定住了。
艾莉有一种不祥的预感。
这个水晶只能检测到Ｃ等級的魔力。

所以別說艾莉原来拥有SSS級的魔力，就連用吊坠变成Ａ等級的魔力触碰後会变成什么样也不知道。
和Ｂ等級一样响起哔哔的声音，然後被认为是Ｂ等級然後结束──这是艾莉原本的预定。
但是，从大家的反应看来，感觉好像比Ｂ等級还要高。

「支部長……这也就是說……？」
「这个孩子……拥有Ｂ等級以上的魔力啊！」

対于阿忒露的问题，支部長颤抖着回答道。
那个瞬間，公会裡的人都深吸了一口气，然後炸开了。

「B等級以上是真的假的……」
「那么小的孩子么……」
「但是实际上看到了水晶炸裂了」
「B等級以上的話，也有可能是Ａ等級么……？」
「不，果然应该没有到那个程度吧……」
「也是啊……」

好像并没有人认为是Ａ等級。
真是太好了，艾莉松了一口气。
反过来說，只要发现是Ｂ等級，就能造成如此程度的骚动。
看来在这个世界，引起骚动的难度微妙的很低呢。
光是跳跃到云层，最高神和天使就会铁青着脸看着的时間点就察觉到了，但是真是想象之上呢。
艾莉才刚变成人类，要如何生活还没有决定好。
想着首先要看看这个世界的时候已经变得太显眼了。
但是，如果因为不想要引人注目而四処都暗藏的話，那就不知为什么而转生成人类了。
控制平衡真是个难题啊。

「要检测Ｂ等級以上的魔力的話，要去王都叫来拥有魔力鑑定技能的人才可以呢……対了，你叫什么名字？」
「啊。艾莉・艾瑪迪斯」

說起来，我还没向支部長作自我介绍呢。

「原来如此，艾莉桑么。先将你作为Ｃ等級魔力持有者来看待吧。但是！正常运转的水晶在接触到你的瞬間就发生了故障而无法测定魔力。那么……和我进行掰手腕対决吧！用魔力強化筋力！」（译者：这裡叫艾莉也是用来君，理由同上，下文不再說明）

随着支部長如此叫喊，周圍响起了欢呼声。

「太棒了！有可能是Ｂ等級的孩子和支部長进行掰手腕！竟然能看到这样的战斗！今天真是太棒了！」

如此感受到的冒険者们骚动起来，不知从哪搬来了桌子，为掰手腕做准備。
接着，艾莉就这么被带到了桌子边，與支部長拳头相互握紧。

「Ready……Go！」

伴随着阿忒露的意外的呼声，掰手腕开始了。
冒険者和公会的职员都很闹腾。
支部長脸颊通红，想要将艾莉打倒。
当然，艾莉则是十分有余裕。
虽說用魔力強化来增強力量，但看起来像用单纯的筋力来进行掰手腕。
现状是，从开始到现在，双方的胳膊都没有颤抖。
那是因为艾莉正在烦恼到底是该赢还是该输呢。
如果战胜了这个城市最強的支部長的話，会十分引人注目。
但是，如果输了的話，可能会被认为因为水晶发生故障，不可以介绍冒険者工作。
如果艾莉打算作为人类继续生活下去，无论如何都需要金钱。
这裡应该不太花俏地赢吧。
这么决定了的艾莉，将支部長的腕压了下去
公会内响起了了一声啪叽的声音。

「呜……呜呀！断了啦！」

支部長的胳膊朝奇怪的方向弯曲了。
骨头折断了。

「啊，不好了！现在就帮你治好呢！」

艾莉慌慌张张朝着在地板上忍受着疼痛的支部長使用了回复魔法。
于是，支部長的胳膊的角度回到了原来的模样。
太好了，太好了，艾莉安心地吐了一口气。
但是。

「治好了……？那么严重的骨折一瞬間就……？多么高等級的回复魔法啊！艾莉桑！你毫无疑问持有Ｂ等級以上魔力！」

支部長握着艾莉的手，熱情的說道。
然後周圍的冒険者也「呜啊啊啊啊啊！」的大声叫喊着。
艾莉察觉到自己失败了，但这已经太迟了。
没想到只是治疗个骨折就大吵大闹的。
而且，在这个城市裡最強的人，那么简单就骨折也是预料之外。

「不仅仅用力量压倒性地战胜了支部長，还会使用那么厉害的回复魔法……Ｂ等級真厉害！」

艾莉自己并不认为这是一件很厉害的事情，但是周圍的人却胡乱地将她抬起。
那时，艾莉第一次学到了一种叫做『恥ずかしい』（害羞）的感情。
脸一下子熱得仿佛要喷出火来。
羞得无地自容，恨不得馬上找个地缝钻进去，于是猛得逃向公会深処。

────────────────────

如果稍微觉得有趣的話能「评價」或者「書签」的話我会很高兴的！