# novel

- title: 姫騎士がクラスメート！　〜異世界チートで奴隷化ハーレム〜
- title_zh: 我的同學是姬騎士
- title_zh2: 姬騎士是同班同學
- author: ＥＫＺ
- source: http://novel18.syosetu.com/n0153ce/
- cover: https://lightnovelstranslations.com/wp-content/uploads/2017/03/Volume-1-cover.jpg
- publisher: syosetu
- date: 2016-10-31T22:05:00+08:00
- status: 連載

## series

- name: 姫騎士がクラスメート！　〜異世界チートで奴隷化ハーレム〜

## preface


```
小田森トオル (Oda-mori tōru), 男高中生 (沒有朋友, 在班上如同空氣般的存在)
修學旅行途中發生意外事故
死後遇到神
以原本的年齡與長相轉生到異世界
成為一名魔隸術師
在異世界抓村娘與女冒險者作為魔隸(性奴隸)
一個月後, 王國因為多起失蹤事件, 而派出姬騎士進行討伐
當姬騎士與魔隸術師見面後
沒想到姬騎士竟然是前世的班級委員(女班長) 

友達のいない男子高校生、小田森トオルは、修学旅行のバス事故でファンタジー異世界へと転生することに。そこで引き当てたジョブは、他人を言いなりにできるチート職業『魔隷術師』。さっそくその力で女たちを奴隷にし始めるトオル。だが同じく転生したクラスメートの美少女、姫野桐華が正義の『姫騎士』としてトオルの前に現れて——！？　「こうなったら元クラスメートも冒険者も、エルフも魔族もお姫様も、まとめて奴隷にしてやるぜ！」【※書籍版１〜３巻、キルタイムコミュニケーション様『ビギニングノベルズ』から発売中！　漫画版『コミックヴァルキリーWeb』にて連載中、コミックス１巻発売中！　書籍版最新４巻、１０／４発売予定！】
```

## tags

- node-novel
- novel18
- syosetu
- チート
- ハーレム
- ファンタジー
- 催眠
- 処女
- 女子高生
- 奴隷
- 男性視点
- 異世界
- 魔法

# contribute

- 神域结城明日奈@tieba
- 零度星塵
- 漆黑黑骑士
- 812843924
- allen01a
- 遥望那一份美丽
- 灬10指葙扣
- 月澪
- GameMastar
- 行動ATM
- 落雨繽紛
- anxian8337
- 斷翼之妖精
- 名無しの紳士さん
- 天野雏菊
- ChrisLiu
- 

# options

## syosetu


## textlayout

- allow_lf2: true

# link

- [dip.jp](https://narou18.dip.jp/search.php?text=n0153ce&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [我的同学是姬骑士吧](https://tieba.baidu.com/f?kw=%E6%88%91%E7%9A%84%E5%90%8C%E5%AD%A6%E6%98%AF%E5%A7%AC%E9%AA%91%E5%A3%AB&ie=utf-8 "我的同学是姬骑士")
- [姫騎士がクラスメート！　THE COMIC](http://www.comic-valkyrie.com/modules/web_valkyrie/classmate/)
- https://www.lightnovel.cn/forum.php?mod=viewthread&tid=922044
- 
